package chernyshov.yuriy.coachassistant;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import chernyshov.yuriy.coachassistant.business.FlurryManager;
import chernyshov.yuriy.coachassistant.business.RateManager;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import com.crittercism.app.Crittercism;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 11/30/12
 * Time: 1:47 PM
 */
public class MainApp extends Application {

    public static String APP_NAME = "";
    // DESTINY OF SCREEN
    public static float DENSITY = DisplayMetrics.DENSITY_DEFAULT;
    public static float RESDENSITY = 0;
    // WIDTH OF THE SCREEN
    public static int SCREEN_WIDTH = 0;
    // HEIGHT OF THE SCREEN
    public static int SCREEN_HEIGHT = 0;
    //OS Version
    public static final int ICE_CREAM_SANDWICH_API_LEVEL = 14;
    public static final int HONEYCOMB_API_LEVEL = 11;
    public static final int FROYO_API_LEVEL = 8;
    public static boolean isGoogleTV = false;

    //singleton instance
    private static volatile MainApp instance = null;
    private static int androidScreenSize = Configuration.SCREENLAYOUT_SIZE_NORMAL;
    private static String buildDateStr = "";
    private static Boolean isDeviceATablet = null;
    private final Handler mUIHandler = new Handler(Looper.getMainLooper());

    private static final int SCREENLAYOUT_SIZE_XLARGE = 4;

    private Activity activeActivity;

    public MainApp() {
        super();
        instance = this;
    }

    /**
     * Double-checked singleton fetching
     * @return
     */
    public static MainApp getInstance() {
        if (instance == null) {
            synchronized(MainApp.class) {
                if (instance == null) {
                    new MainApp();
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initMainAppVars();

        Logger.initLogger(this, true);
        Logger.i("+++ Create Main App +++");
        Logger.i("- processors: " + Runtime.getRuntime().availableProcessors());
        Logger.i("- version: " + AppUtilities.getApplicationVersion() + ", " + buildDateStr);
        Logger.i("- screen size: " + getScreenSizeAsString() + ", DPI: " + getDPIAsString() + ", OS ver: " + Build.VERSION.RELEASE + ", API lvl: " + Build.VERSION.SDK_INT);
        double diagonal = getDeviceScreenSizeInInches();
        Logger.i("Is Tablet UI: " + isTabletUI() + ", device screen diagonal: " + diagonal + " inch(es)");

        FlurryManager.initFlurry(true);
        Crittercism.init(getApplicationContext(), "5125df6b59e1bd1e930008f2");

        RateManager.getInstance().init();
    }

    @TargetApi(8)
    public File getExternalFilesDirAPI8(String type) {
        return super.getExternalFilesDir(type);
    }

    public void setActiveActivity(Activity activeActivity) {
        this.activeActivity = null;
        this.activeActivity = activeActivity;
    }

    public Activity getActiveActivity() {
        return activeActivity;
    }

    public void runInUIThread(Runnable r) {
        mUIHandler.post(r);
    }

    private void initMainAppVars() {
        try {
            Resources res = getResources();
            APP_NAME = res.getString(R.string.app_name);
            DisplayMetrics displayMetrics = res.getDisplayMetrics();
            DENSITY = displayMetrics.densityDpi;
            RESDENSITY = displayMetrics.density;
            SCREEN_WIDTH = displayMetrics.widthPixels;
            SCREEN_HEIGHT = displayMetrics.heightPixels;

            PackageManager packageManager = getPackageManager();
            isGoogleTV = (packageManager != null && packageManager.hasSystemFeature("com.google.android.tv"));

            buildDateStr = AppUtilities.readBuildDate();
            calculateIsTabletUi();
            cacheDeviceScreenSize();

        } catch (Throwable th) {
            Logger.e("Fatal error while init main app variables!", th);
            APP_NAME = "+COACH+ASSISTANT+";
        }
    }

    private static String getScreenSizeAsString() {
        switch (androidScreenSize) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small_screen";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal_screen";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large_screen";
            case SCREENLAYOUT_SIZE_XLARGE:
                return "xlarge_screen";
        }
        return "unknown";
    }

    private static String getDPIAsString() {
        if (DENSITY == DisplayMetrics.DENSITY_LOW) {
            return "ldpi";
        } else if (DENSITY == DisplayMetrics.DENSITY_MEDIUM) {
            return "mdpi";
        } else if (DENSITY == DisplayMetrics.DENSITY_HIGH) {
            return "hdpi";
        } else if (DENSITY == 320) { // we can't use API level 7 in order to use DENSITY_XHIGH constant
            return "xhdpi";
        } else {
            return "unknown dpi: " + DENSITY;
        }
    }

    private static boolean isTabletUI() {
        if (isDeviceATablet == null){
            throw new NullPointerException("calculateIsTabletUi should be invoked on on start of application before method isTabletUI");
        }
        return isDeviceATablet;
    }

    private static void calculateIsTabletUi(){
        double diagonal = getDeviceScreenSizeInInches();
        // Tablet devices should have a screen diagonal greater than 6 inches
        //Logger.i("Device screen diagonal: " + diagonal + " inch(es)");
        isDeviceATablet = diagonal >= 6.0 && isRunningHoneycomb();
        isDeviceATablet = isDeviceATablet || isGoogleTV;
    }

    private static boolean isRunningHoneycomb() {
        return Build.VERSION.SDK_INT >= HONEYCOMB_API_LEVEL;
    }

    private static double getDeviceScreenSizeInInches() {
        try {
            DisplayMetrics dm = getInstance().getResources().getDisplayMetrics();
            float screenWidth  = dm.widthPixels / dm.xdpi;
            float screenHeight = dm.heightPixels / dm.ydpi;
            return Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2));
        } catch (Throwable exception) {
            Logger.e("Failed to compute screen size", exception);
            return 0;
        }
    }

    private static void cacheDeviceScreenSize() {
        androidScreenSize = getInstance().getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
    }
}