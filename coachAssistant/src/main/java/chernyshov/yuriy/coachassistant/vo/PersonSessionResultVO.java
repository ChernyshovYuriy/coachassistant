package chernyshov.yuriy.coachassistant.vo;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 12/11/12
 * Time: 11:13 PM
 */
public class PersonSessionResultVO implements Serializable {
    public long timestamp;
    public byte result;
    public boolean wasAtSession;
}