package chernyshov.yuriy.coachassistant.vo;

import android.provider.BaseColumns;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/28/12
 * Time: 2:39 PM
 */
public class AppEntry implements BaseColumns {
    public static final String COLUMN_NAME_NULLABLE = "null_name";
    public static final String COLUMN_NAME_MAIN_ICON_URL = "main_icon_url";
    public static final String COLUMN_NAME_LAST_USED_GROUP = "last_used_group";
    public static final String COLUMN_NAME_TITLE = "app_table_title";
    public static final String COLUMN_NAME_RATE_DIALOG_COUNTER = "rate_dialog_counter";
}