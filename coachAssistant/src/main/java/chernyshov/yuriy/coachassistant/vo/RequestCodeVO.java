package chernyshov.yuriy.coachassistant.vo;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 12/28/12
 * Time: 12:05 AM
 */
public class RequestCodeVO {
    public static short MAIN_IMG_CAMERA_RESULT = 2001;
    public static short MAIN_IMG_GALLERY_RESULT = 2002;
    public static short PERSON_IMG_CAMERA_RESULT = 2003;
    public static short PERSON_IMG_GALLERY_RESULT = 2004;
    public static short GROUP_INFO_IMG_CAMERA_RESULT = 2005;
    public static short GROUP_INFO_IMG_GALLERY_RESULT = 2006;
}