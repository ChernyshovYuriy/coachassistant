package chernyshov.yuriy.coachassistant.vo;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 02.12.12
 * Time: 15:03
 */
public class ExtrasVO {

    public static final String GROUP_NAME = "GROUP_NAME";
    public static final String PERSON_ID = "PERSON_ID";
    public static final String PERSON_HASH = "PERSON_HASH";
    public static final String HISTORY_TIMESTAMP_STRING = "HISTORY_TIMESTAMP_STRING";
    public static final String HISTORY_TIMESTAMP = "HISTORY_TIMESTAMP";
    public static final String CONTACT_ID = "CONTACT_ID";
}