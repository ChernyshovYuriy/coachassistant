package chernyshov.yuriy.coachassistant.vo;

import android.provider.BaseColumns;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 30.11.12
 * Time: 23:04
 */
public class GroupEntry implements BaseColumns {
    public static final String COLUMN_NAME_NULLABLE = "null_name";
    public static final String COLUMN_NAME_TITLE = "title";
    public static final String COLUMN_NAME_IMG_URL = "img_url";
    public static final String COLUMN_NAME_DESCRIPTION = "description";
}