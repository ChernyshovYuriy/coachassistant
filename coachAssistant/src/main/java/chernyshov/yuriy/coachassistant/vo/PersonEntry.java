package chernyshov.yuriy.coachassistant.vo;

import android.provider.BaseColumns;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/4/12
 * Time: 5:16 PM
 */
public class PersonEntry implements BaseColumns {
    public static final String COLUMN_NAME_NULLABLE = "null_name";
    public static final String COLUMN_NAME_TITLE = "title";
    public static final String COLUMN_NAME_RESULT = "result";
    public static final String COLUMN_NAME_GROUP = "group_name";
    public static final String COLUMN_NAME_IMG_URL = "img_url";
    public static final String COLUMN_NAME_CONTACT_THUMB_URI = "contact_thumb_uri";
    public static final String COLUMN_NAME_PHONE = "phone";
    public static final String COLUMN_NAME_EMAIL = "email";
    public static final String COLUMN_NAME_ADDRESS = "address";
    public static final String COLUMN_NAME_DESCRIPTION = "description";
    public static final String COLUMN_NAME_PRESENTS = "presents";
}