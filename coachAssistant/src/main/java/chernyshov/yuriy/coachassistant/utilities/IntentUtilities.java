package chernyshov.yuriy.coachassistant.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.text.format.Time;

import java.io.File;
import java.io.IOException;
import java.util.List;

import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.views.SafeToast;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/29/12
 * Time: 10:06 AM
 */
public class IntentUtilities {

    public static Uri createGetPictureIntent(Context context, Activity activity, short requestCode) {
        // http://developer.android.com/training/camera/photobasics.html
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (isIntentAvailable(context, takePictureIntent)) {
            String storageState = Environment.getExternalStorageState();
            if (TextUtils.equals(storageState, Environment.MEDIA_MOUNTED)) {
                Time curTime = new Time();
                curTime.setToNow();
                String externalStorageDir = AppUtilities.getExternalStorageDir();
                if (TextUtils.isEmpty(externalStorageDir)) {
                    if (!activity.isFinishing()) {
                        SafeToast.showToastAnyThread(context.getString(R.string.create_intent_error));
                    }
                    return null;
                }
                String path = AppUtilities.combinePath(externalStorageDir, "files", curTime.format2445() + ".jpg");
                File file = new File(path);
                try {
                    if (!file.exists()) {
                        file.getParentFile().mkdirs();
                        file.createNewFile();
                    }
                } catch (IOException e) {
                    Logger.e("Take picture file creation error", e);
                }
                Uri uploadPictureFileUri = Uri.fromFile(file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uploadPictureFileUri);
                activity.startActivityForResult(takePictureIntent, requestCode);
                return uploadPictureFileUri;
            } else {
                if (!activity.isFinishing()) {
                    SafeToast.showToastAnyThread(context.getString(R.string.sd_card_required));
                }
                return null;
            }
        } else {
            return null;
        }
    }

    private static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private static boolean isIntentAvailable(Context context, Intent intent) {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }
}