package chernyshov.yuriy.coachassistant.utilities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 1/19/13
 * Time: 6:30 PM
 */
public class DataBaseUtilities {

    public static void dumpDatabaseColumns(SQLiteDatabase db, String tableName) {
        Cursor cursor = null;
        String[] colNames;
        try {
            cursor = db.rawQuery("SELECT * FROM " + tableName + " LIMIT 1", null);
            colNames = cursor.getColumnNames();
        } catch (Throwable throwable) {
            Logger.e("DataBaseUtilities dumpDatabaseColumns error: ", throwable);
            return;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Logger.d("- table name: " + tableName);
        for (String colName : colNames) {
            Logger.d(" - column: " + colName);
        }
    }

    public static int rowsCount(SQLiteDatabase db, String tableName) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + tableName + " LIMIT 1", null);
            return cursor.getCount();
        } catch (Throwable throwable) {
            Logger.e("DataBaseUtilities rowsCount error:", throwable);
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}