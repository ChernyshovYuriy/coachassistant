package chernyshov.yuriy.coachassistant.model;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 9/29/13
 * Time: 2:38 PM
 */
public class ImportedPerson {

    private String displayName;
    private String phoneNumber;
    private String email;
    private String contactThumbUri;

    public String getContactThumbUri() {
        return contactThumbUri;
    }

    public void setContactThumbUri(String contactThumbUri) {
        this.contactThumbUri = contactThumbUri;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}