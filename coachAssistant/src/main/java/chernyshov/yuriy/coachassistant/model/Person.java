package chernyshov.yuriy.coachassistant.model;

import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 03.12.12
 * Time: 22:04
 */
public class Person {

    private String displayName;
    private String phone;
    private String pictureURL;
    private String contactThumbURI;
    private String email;
    private String address;
    private String description;
    //private int id;
    private long selectedHistoryTimestamp;
    private ArrayList<PersonSessionResultVO> results;

    public Person() {

    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getContactThumbURI() {
        return contactThumbURI;
    }

    public void setContactThumbURI(String contactThumbURI) {
        this.contactThumbURI = contactThumbURI;
    }

    public ArrayList<PersonSessionResultVO> getResults() {
        return results;
    }

    public void setResults(ArrayList<PersonSessionResultVO> results) {
        this.results = results;
    }

    public long getSelectedHistoryTimestamp() {
        return selectedHistoryTimestamp;
    }

    public void setSelectedHistoryTimestamp(long selectedHistoryTimestamp) {
        this.selectedHistoryTimestamp = selectedHistoryTimestamp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}