package chernyshov.yuriy.coachassistant.business.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import chernyshov.yuriy.coachassistant.model.Group;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.*;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/13/12
 * Time: 1:15 PM
 */
public final class GroupsDataBaseManager {

    public static ArrayList<String> readGroups(final Context context) {
        final DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        final SQLiteDatabase database;
        try {
            database = dataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB readGroups", e);
            return null;
        }
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        final String[] projection = {
                GroupEntry._ID,
                GroupEntry.COLUMN_NAME_TITLE
        };
        // How you want the results sorted in the resulting Cursor
        final String sortOrder = GroupEntry.COLUMN_NAME_TITLE + " COLLATE NOCASE";
        String selection = null;
        String[] selectionArgs = null;
        Cursor cursor = null;
        final ArrayList<String> result = new ArrayList<>();
        try {
            //noinspection ConstantConditions
            cursor = database.query(
                    DataBaseConstants.GROUPS_TABLE_NAME,      // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    result.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } catch (Throwable throwable) {
            Logger.e("Error readGroups", throwable);
        }  finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    database.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB readGroups", throwable);
                }
            }
        }
        return result;
    }

    public static Group getGroup(Context context, String groupName) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB getGroup", e);
            return null;
        }
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                GroupEntry._ID,
                GroupEntry.COLUMN_NAME_TITLE,
                GroupEntry.COLUMN_NAME_IMG_URL,
                GroupEntry.COLUMN_NAME_DESCRIPTION
        };
        Group group = null;
        String selection = null;
        String[] selectionArgs = null;
        Cursor cursor = null;
        try {
            //noinspection ConstantConditions
            cursor = db.query(
                    DataBaseConstants.GROUPS_TABLE_NAME,      // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    null                                      // The sort order
            );
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    if (TextUtils.equals(cursor.getString(1), groupName)) {
                        group = new Group();
                        group.setImageURL(cursor.getString(2));
                        group.setDescription(cursor.getString(3));
                        break;
                    }
                }
                while (cursor.moveToNext());
            }
        } catch (Throwable throwable) {
            Logger.e("Error getGroup", throwable);
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    db.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB getGroup", throwable);
                }
            }
        }
        return group;
    }

    public static void setGroupParams(Context context, String groupName, ContentValues contentValues) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB setGroupParams", e);
            return;
        }
        String selection = GroupEntry.COLUMN_NAME_TITLE + "=?";
        String[] selectionArgs = { groupName };
        try {
            int result = db.update(
                    DataBaseConstants.GROUPS_TABLE_NAME,
                    contentValues,
                    selection,
                    selectionArgs);
            Logger.d("Update Group parameter " + contentValues + " result " + result);
        } catch (Throwable throwable) {
            Logger.e("Error setGroupParams", throwable);
        } finally {
            try {
                db.close();
            }  catch (Throwable throwable) {
                Logger.e("Error close DB setGroupParams", throwable);
            }
        }
    }

    public static int addGroup(String groupName, Context context) {
        if (!TextUtils.isEmpty(groupName)) {
            DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
            // Gets the data repository in write mode
            SQLiteDatabase db;
            try {
                db = mDbHelperGroups.getWritableDatabase();
            } catch (SQLiteException e) {
                Logger.e("Error open DB addGroup", e);
                return -1;
            }
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(GroupEntry.COLUMN_NAME_TITLE, groupName);
            // Insert the new row, returning the primary key value of the new row
            long newRowId = -1;
            try {
                newRowId = db.insert(
                        DataBaseConstants.GROUPS_TABLE_NAME,
                        GroupEntry.COLUMN_NAME_NULLABLE,
                        values);
            } catch (Throwable throwable) {
                Logger.e("Error addGroup", throwable);
            }  finally {
                try {
                    db.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB addGroup", throwable);
                }
            }
            return (int) newRowId;
        } else {
            return -1;
        }
    }

    public static int deleteGroup(String groupNameToDelete, Context context) {
        if (!TextUtils.isEmpty(groupNameToDelete)) {
            DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
            // Gets the data repository in write mode
            SQLiteDatabase db;
            try {
                db = mDbHelperGroups.getWritableDatabase();
            } catch (SQLiteException e) {
                Logger.e("Error open DB deleteGroup", e);
                return -1;
            }
            int deletedRowId = -1;
            try {
                deletedRowId = db.delete(DataBaseConstants.GROUPS_TABLE_NAME, GroupEntry.COLUMN_NAME_TITLE + "='" + groupNameToDelete + "'", null);
                if (deletedRowId > 0) {
                    deletePersonsOfTheGroup(groupNameToDelete, context);
                }
            } catch (Throwable throwable) {
                Logger.e("Error deleteGroup", throwable);
            }  finally {
                try {
                    db.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB deleteGroup", throwable);
                }
            }
            return deletedRowId;
        } else {
            return  -1;
        }
    }

    private static void deletePersonsOfTheGroup(String deletedGroupName, Context context) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB deletePersonsOfTheGroup", e);
            return;
        }
        try {
            db.delete(DataBaseConstants.PERSONS_TABLE_NAME, PersonEntry.COLUMN_NAME_GROUP + "='" + deletedGroupName + "'", null);
        } catch (Throwable throwable) {
            Logger.e("Error deletePersonsOfTheGroup", throwable);
        }  finally {
            try {
                db.close();
            }  catch (Throwable throwable) {
                Logger.e("Error close DB deletePersonsOfTheGroup", throwable);
            }
        }
    }
}