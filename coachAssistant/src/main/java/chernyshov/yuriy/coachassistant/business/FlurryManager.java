package chernyshov.yuriy.coachassistant.business;

import android.content.Context;
import android.text.TextUtils;

import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.Map;

import chernyshov.yuriy.coachassistant.utilities.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 11.01.13
 * Time: 17:27
 */
public class FlurryManager {

    public static final String EVENT_ID_GROUP_SELECTED = "group-selected";
    public static final String EVENT_ID_SESSION_SELECTED = "session-selected";
    public static final String EVENT_ID_HISTORY_SELECTED = "history-selected";
    public static final String EVENT_ID_PERSON_INFO_SELECTED = "person-info-selected";
    public static final String EVENT_ID_PERSON_INFO_CALL_SELECTED = "person-info-call-selected";
    public static final String EVENT_ID_SHOW_RATE_DIALOG = "show-rate-dialog";

    private static final String FLURRY_API_KEY = "FTSC47C5KXJ37WX9F9Y2";

    private static boolean isClientStatsCollecting = false;

    public static void initFlurry(boolean enabled) {
        isClientStatsCollecting = enabled;
        FlurryAgent.setLogLevel(0);
        FlurryAgent.setLogEnabled(enabled);
        FlurryAgent.setLogEvents(enabled);
        FlurryAgent.setUseHttps(true);
        FlurryAgent.setReportLocation(false);
        Logger.d("FLURRY enabled: " + enabled);
    }

    public static void startOrContinueFlurrySession(Context context, boolean incPageView) {
        if (!isClientStatsCollecting) {
            return;
        }
        try {
            FlurryAgent.onStartSession(context, FLURRY_API_KEY);
            if (incPageView) {
                // using Pageviews for counting views of any Activity in app
                FlurryAgent.onPageView();
            }
        } catch (Throwable throwable) {
            Logger.w("FLURRY Can't startRecording Flurry session for: " + context.getClass().getSimpleName(), throwable);
        }
    }

    public static void endFlurrySession(Context context) {
        try {
            FlurryAgent.onEndSession(context);
        } catch (Throwable throwable) {
            Logger.w("FLURRY Can't end Flurry session for: " + context.getClass().getSimpleName(), throwable);
        }
    }

    public static void logEvent(String eventId) {
        logEvent(eventId, null);
    }

    public static void logEvent(String eventId, String paramKey, String paramValue) {
        if (TextUtils.isEmpty(paramKey)) {
            Logger.w("FLURRY logEvent() called with empty key for eventID: " + eventId);
            return;
        }
        if (paramValue == null) {
            paramValue = "null";
        }
        Map<String, String> params = new HashMap<String, String>(1);
        params.put(paramKey, paramValue);
        logEvent(eventId, params);
    }

    public static void logEvent(String eventId, Map<String,String> params) {
        if (!isClientStatsCollecting) {
            return;
        }
        if (params == null) {
            FlurryAgent.onEvent(eventId);
            //Logger.d("FLURRY onEvent: eventId = " + eventId);
        } else {
            FlurryAgent.onEvent(eventId, params);
            //Logger.d("FLURRY onEvent: eventId = " + eventId + ", key = " + paramKey + ", value = " + paramValue);
        }
    }
}