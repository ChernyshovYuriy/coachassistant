package chernyshov.yuriy.coachassistant.business.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.AppEntry;
import chernyshov.yuriy.coachassistant.vo.GroupEntry;
import chernyshov.yuriy.coachassistant.vo.PersonEntry;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/28/12
 * Time: 2:37 PM
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 3;

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String BLOB_TYPE = " BLOB";
    private static final String DEFAULT = " DEFAULT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_APP_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + DataBaseConstants.APP_TABLE_NAME + " (" +
                    AppEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    AppEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    AppEntry.COLUMN_NAME_MAIN_ICON_URL + TEXT_TYPE + COMMA_SEP +
                    AppEntry.COLUMN_NAME_LAST_USED_GROUP + TEXT_TYPE + COMMA_SEP +
                    AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER + INTEGER_TYPE + DEFAULT + " 0" + // v.3
                    " )";
    private static final String SQL_CREATE_GROUPS_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + DataBaseConstants.GROUPS_TABLE_NAME + " (" +
                    GroupEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +    // v.1
                    GroupEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +   // v.1
                    GroupEntry.COLUMN_NAME_IMG_URL + TEXT_TYPE + COMMA_SEP + // v.1
                    GroupEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE +         // v.1
                    " )";
    private static final String SQL_CREATE_PERSONS_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + DataBaseConstants.PERSONS_TABLE_NAME + " (" +
                    PersonEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +       // v.1
                    PersonEntry.COLUMN_NAME_GROUP + TEXT_TYPE + COMMA_SEP +       // v.1
                    PersonEntry.COLUMN_NAME_RESULT + BLOB_TYPE + COMMA_SEP +      // v.1
                    PersonEntry.COLUMN_NAME_IMG_URL + TEXT_TYPE + COMMA_SEP +     // v.1
                    PersonEntry.COLUMN_NAME_PHONE + TEXT_TYPE + COMMA_SEP +       // v.1
                    PersonEntry.COLUMN_NAME_EMAIL + TEXT_TYPE + COMMA_SEP +       // v.1
                    PersonEntry.COLUMN_NAME_ADDRESS + TEXT_TYPE + COMMA_SEP +     // v.1
                    PersonEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP + // v.1
                    PersonEntry.COLUMN_NAME_CONTACT_THUMB_URI + TEXT_TYPE +       // v.2
                    " )";

    public DataBaseHelper(Context context) {
        super(context, DataBaseConstants.APP_DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_APP_ENTRIES);
        Logger.i("Table " + DataBaseConstants.APP_TABLE_NAME + " created");
        db.execSQL(SQL_CREATE_GROUPS_ENTRIES);
        Logger.i("Table " + DataBaseConstants.GROUPS_TABLE_NAME + " created");
        db.execSQL(SQL_CREATE_PERSONS_ENTRIES);
        Logger.i("Table " + DataBaseConstants.PERSONS_TABLE_NAME + " created");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.d("DatabaseHelper > onUpgrade from " + oldVersion + " to " + newVersion);
        for (int i = oldVersion + 1; i <= newVersion; i++) {
            switch (i) {
                case 2:
                    to2(db);
                    break;
                case 3:
                    to3(db);
                    break;
            }
        }
    }

    private void to2(SQLiteDatabase db) {
        Logger.d("DatabaseHelper > to2");
        db.execSQL("ALTER TABLE " + DataBaseConstants.PERSONS_TABLE_NAME + " ADD " +
                PersonEntry.COLUMN_NAME_CONTACT_THUMB_URI + " " + TEXT_TYPE);
    }

    private void to3(SQLiteDatabase db) {
        Logger.d("DatabaseHelper > to3");
        db.execSQL("ALTER TABLE " + DataBaseConstants.APP_TABLE_NAME + " ADD " +
                AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER + " " + INTEGER_TYPE + DEFAULT + " 0");
    }

    public static void dumpCursor(Cursor cursor) {
        if (cursor == null) {
            Logger.e("Dump Cursor stop, null");
            return;
        }
        cursor.moveToFirst();
        Logger.d("Dump Cursor");
        if (!cursor.isAfterLast()) {
            do {
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getColumnName(i).equals(PersonEntry.COLUMN_NAME_RESULT)) {
                        Logger.d(" - " + cursor.getColumnName(i) + " " + Arrays.toString(cursor.getBlob(i)));
                    } else {
                        Logger.d(" - " + cursor.getColumnName(i) + " " + cursor.getString(i));
                    }
                }
            } while (cursor.moveToNext());
        }
    }

    public static void dumpDatabase(Context context, String databaseName) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB readGroups", e);
            return;
        }
        // Define a projection that specifies which columns from the database you will actually use after this query.
        String[] projection = null;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        String selection = null;
        String[] selectionArgs = null;
        Cursor cursor = null;
        try {
            //noinspection ConstantConditions
            cursor = db.query(
                    databaseName,                             // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );
            dumpCursor(cursor);
        } catch (Throwable throwable) {
            Logger.e("Error readGroups", throwable);
        }  finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    db.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB readGroups", throwable);
                }
            }
        }
    }
}