package chernyshov.yuriy.coachassistant.business.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.PersonEntry;
import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 1/16/13
 * Time: 7:42 PM
 */
public final class PersonsDataBaseManager {

    private PersonsDataBaseManager() { }

    public static Person getPerson(final int aPersonHash, final Context context) {
        final DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        final SQLiteDatabase database;
        try {
            database = dataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB getPerson", e);
            return null;
        }
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        final String[] projection = {
                PersonEntry._ID,
                PersonEntry.COLUMN_NAME_TITLE,
                PersonEntry.COLUMN_NAME_RESULT,
                PersonEntry.COLUMN_NAME_GROUP,
                PersonEntry.COLUMN_NAME_IMG_URL,
                PersonEntry.COLUMN_NAME_PHONE,
                PersonEntry.COLUMN_NAME_EMAIL,
                PersonEntry.COLUMN_NAME_ADDRESS,
                PersonEntry.COLUMN_NAME_DESCRIPTION,
                PersonEntry.COLUMN_NAME_CONTACT_THUMB_URI
        };
        Person person = null;
        String selection = null;
        String[] selectionArgs = null;
        Cursor cursor = null;
        try {
            //noinspection ConstantConditions
            cursor = database.query(
                    DataBaseConstants.PERSONS_TABLE_NAME,     // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    null                                      // The sort order
            );
            //Logger.d(" - persons count " + cursor.getCount());
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    final int personHash = AppUtilities.getMD5fromString(
                            cursor.getString(1) + cursor.getString(3)
                    ).hashCode();
                    if (personHash == aPersonHash) {
                        person = new Person();
                        person.setDisplayName(cursor.getString(1));
                        person.setPictureURL(cursor.getString(4));
                        person.setPhone(cursor.getString(5));
                        person.setAddress(cursor.getString(7));
                        person.setDescription(cursor.getString(8));
                        person.setContactThumbURI(cursor.getString(9));
                        //person.setId(personId);
                        break;
                    }
                }
                while (cursor.moveToNext());
            }
        } catch (Throwable throwable) {
            Logger.e("Error getPerson", throwable);
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    database.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB getPerson", throwable);
                }
            }
        }
        return person;
    }

    public static ArrayList<Person> getPersons(final String groupName, final Context context) {
        final DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        final SQLiteDatabase database;
        try {
            database = dataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB getPersons", e);
            return null;
        }
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        final String[] projection = {
                PersonEntry._ID,
                PersonEntry.COLUMN_NAME_TITLE,
                PersonEntry.COLUMN_NAME_RESULT,
                PersonEntry.COLUMN_NAME_GROUP,
                PersonEntry.COLUMN_NAME_IMG_URL,
                PersonEntry.COLUMN_NAME_CONTACT_THUMB_URI
        };
        // How you want the results sorted in the resulting Cursor
        final String sortOrder = PersonEntry.COLUMN_NAME_RESULT + " COLLATE NOCASE DESC";
        String selection = null;
        String[] selectionArgs = null;
        Cursor cursor = null;
        ArrayList<Person> result = new ArrayList<Person>();
        try {
            //noinspection ConstantConditions
            cursor = database.query(
                    DataBaseConstants.PERSONS_TABLE_NAME,     // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );
            Logger.d(" - persons count " + cursor.getCount());
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    if (!cursor.getString(3).equals(groupName)) {
                        continue;
                    }
                    //Logger.d(" - person name '" + cursor.getString(1) + "'");
                    final Person person = new Person();
                    person.setDisplayName(cursor.getString(1));
                    final ObjectInputStream ois = new ObjectInputStream(
                            new ByteArrayInputStream(cursor.getBlob(2))
                    );
                    try {
                        @SuppressWarnings("unchecked")
                        final ArrayList<PersonSessionResultVO> list
                                = (ArrayList<PersonSessionResultVO>) ois.readObject();
                        //Logger.d(" - person vo " + list);
                        person.setResults(list);
                    } finally {
                        ois.close();
                    }
                    person.setPictureURL(cursor.getString(4));
                    person.setContactThumbURI(cursor.getString(5));
                    result.add(person);
                }
                while (cursor.moveToNext());
            }
        } catch (Throwable throwable) {
            Logger.e("Error getPersons", throwable);
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    database.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB getPersons", throwable);
                }
            }
        }
        return result;
    }

    public static Person addPerson(final String personName, final String groupName,
                                   final Context context) {
        if (TextUtils.isEmpty(groupName)) {
            Logger.e("addPerson - group name empty");
            return null;
        }
        if (!TextUtils.isEmpty(personName)) {
            DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
            // Gets the data repository in write mode
            final SQLiteDatabase database;
            try {
                database = mDbHelperGroups.getWritableDatabase();
            } catch (SQLiteException e) {
                Logger.e("Error open DB addPerson", e);
                return null;
            }
            final ArrayList<PersonSessionResultVO> arrayList = new ArrayList<>();
            byte[] buff = AppUtilities.arrayListToByteArray(arrayList);
            if (buff == null) {
                return null;
            }
            // Create a new map of persons, where column names are the keys
            final ContentValues values = new ContentValues();
            values.put(PersonEntry.COLUMN_NAME_TITLE, personName);
            values.put(PersonEntry.COLUMN_NAME_RESULT, buff);
            values.put(PersonEntry.COLUMN_NAME_GROUP, groupName);
            // Insert the new row, returning the primary key value of the new row
            long newRowId = -1;
            try {
                newRowId = database.insert(
                        DataBaseConstants.PERSONS_TABLE_NAME,
                        PersonEntry.COLUMN_NAME_NULLABLE,
                        values);
            } catch (Throwable throwable) {
                Logger.e("Error addPerson", throwable);
            }  finally {
                try {
                    database.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB addPerson", throwable);
                }
            }
            if (newRowId != -1) {
                Person person = new Person();
                person.setDisplayName(personName);
                person.setResults(arrayList);
                //person.setId(personName.hashCode());
                return person;
            }
        } else {
            return null;
        }
        return null;
    }

    public static int deletePerson(final String personNameToDelete, final Context context) {
        if (!TextUtils.isEmpty(personNameToDelete)) {
            final DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
            // Gets the data repository in write mode
            final SQLiteDatabase database;
            try {
                database = mDbHelperGroups.getWritableDatabase();
            } catch (SQLiteException e) {
                Logger.e("Error open DB deletePerson", e);
                return -1;
            }
            int deletedRowId = -1;
            try {
                deletedRowId = database.delete(
                        DataBaseConstants.PERSONS_TABLE_NAME,
                        PersonEntry.COLUMN_NAME_TITLE + "='" + personNameToDelete + "'",
                        null
                );
            } catch (Throwable throwable) {
                Logger.e("Error deletePerson", throwable);
            }  finally {
                try {
                    database.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB deletePerson", throwable);
                }
            }
            return deletedRowId;
        } else {
            return  -1;
        }
    }

    public static boolean updatePersonDetails(final String personName, final Context context,
                                              final ContentValues values) {
        final DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        final SQLiteDatabase database;
        try {
            database = dataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB updatePersonDetails", e);
            return false;
        }
        // Which row to update, based on the ID
        final String selection = PersonEntry.COLUMN_NAME_TITLE + "=?";
        final String[] selectionArgs = { personName };
        try {
            final int count = database.update(
                    DataBaseConstants.PERSONS_TABLE_NAME,
                    values,
                    selection,
                    selectionArgs
            );
            Logger.d("Update person DB " + count + " " + values);
            if (count == 1) {
                return true;
            } else if (count > 1) {
                Logger.w("update result > 1 with personName '" + personName + "'");
                return false;
            }
        } catch (Throwable throwable) {
            Logger.e("Error updatePersonDetails", throwable);
            return false;
        } finally {
            try {
                database.close();
            }  catch (Throwable throwable) {
                Logger.e("Error close DB updatePersonDetails", throwable);
            }
        }
        return false;
    }
}