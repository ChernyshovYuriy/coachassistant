package chernyshov.yuriy.coachassistant.business.views;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/19/12
 * Time: 5:11 PM
 */
public abstract class AppBaseAdapterData<T> {

    private final List<T> mItems;

    public AppBaseAdapterData(Comparator<T> comparator) {
        if (comparator != null) {
            // TODO:
            mItems = new ArrayList<T>();
        } else {
            mItems = new ArrayList<T>();
        }
    }

    public void addItem(T item) {
        mItems.add(item);
    }

    public T getItem(int position) {
        return mItems.get(position);
    }

    public Iterator<T> itemsIterator() {
        return mItems.iterator();
    }

    public boolean removeItem(T item) {
        return mItems.remove(item);
    }

    public void clearItems() {
        mItems.clear();
    }

    public int getItemsCount() {
        return mItems.size();
    }
}