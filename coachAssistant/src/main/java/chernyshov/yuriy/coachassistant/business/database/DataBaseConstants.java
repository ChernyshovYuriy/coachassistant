package chernyshov.yuriy.coachassistant.business.database;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/7/12
 * Time: 12:48 PM
 */
public class DataBaseConstants {
    public static final String APP_DATABASE_NAME = "CoachAssistant.db";
    public static final String GROUPS_TABLE_NAME = "groups";
    public static final String PERSONS_TABLE_NAME = "persons";
    public static final String APP_TABLE_NAME = "app";
}