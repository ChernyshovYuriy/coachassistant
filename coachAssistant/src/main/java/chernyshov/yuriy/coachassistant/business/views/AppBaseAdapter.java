package chernyshov.yuriy.coachassistant.business.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import chernyshov.yuriy.coachassistant.views.GroupBaseActivity;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/19/12
 * Time: 4:52 PM
 */
public abstract class AppBaseAdapter<T, V> extends BaseAdapter {

    protected final GroupBaseActivity mCurrentActivity;

    private final AppBaseAdapterData<T> mAdapterData;
    protected V mViewHolder;

    public AppBaseAdapter(AppBaseAdapterData<T> aData, GroupBaseActivity aCurrentActivity) {
        super();

        if (aData == null) {
            throw new NullPointerException("Can't create " + getClass().getSimpleName() + " with NULL aData");
        }
        if (aCurrentActivity == null) {
            throw new NullPointerException("Can't create " + getClass().getSimpleName() + " with NULL aCurrentActivity");
        }

        mCurrentActivity = aCurrentActivity;
        mAdapterData = aData;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    /**
     * Method should instantiate ViewHolder object of correct class and fill
     * it fields by links to corresponding views.
     *
     * @param view root view to search corresponding sub-views
     */
    public abstract V createViewHolder(View view);

    protected View prepareViewAndHolder(View convertView, int inflateViewResId) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mCurrentActivity).inflate(inflateViewResId, null);
            mViewHolder = createViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            //noinspection unchecked
            mViewHolder = (V) convertView.getTag();
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return mAdapterData.getItemsCount();
    }

    @Override
    public Object getItem(int position) {
        if (position >= 0 && position < mAdapterData.getItemsCount()) {
            return mAdapterData.getItem(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}