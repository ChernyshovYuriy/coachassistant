package chernyshov.yuriy.coachassistant.business.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import chernyshov.yuriy.coachassistant.utilities.DataBaseUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.AppEntry;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 1/16/13
 * Time: 7:50 PM
 */
public class AppSettingsDataBaseManager {

    private static final String APP_SETTINGS_TABLE_NAME = "AppSettingsTable";

    public static String getAppParameter_String(Context context, String parameterName) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB getAppParameter_String", e);
            return null;
        }
        // Which row to update, based on the ID
        String selection = AppEntry.COLUMN_NAME_TITLE + "=?";
        String[] selectionArgs = { APP_SETTINGS_TABLE_NAME };
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                AppEntry.COLUMN_NAME_TITLE,
                parameterName
        };
        Cursor cursor = null;
        String result = null;
        try {
            cursor = db.query(
                    DataBaseConstants.APP_TABLE_NAME, // The table to query
                    projection,                       // The columns to return
                    selection,                        // The columns for the WHERE clause
                    selectionArgs,                    // The values for the WHERE clause
                    null,                             // don't group the rows
                    null,                             // don't filter by row groups
                    null                              // The sort order
            );
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    result = cursor.getString(1);
                } while (cursor.moveToNext());
            }
        } catch (Throwable throwable) {
            Logger.e("Error getAppParameter_String", throwable);
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    db.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB getAppParameter_String", throwable);
                }
            }
        }
        return result;
    }

    public static int getAppParameter_int(Context context, String parameterName) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB getAppParameter_Int", e);
            return 0;
        }
        // Which row to update, based on the ID
        String selection = AppEntry.COLUMN_NAME_TITLE + "=?";
        String[] selectionArgs = { APP_SETTINGS_TABLE_NAME };
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                AppEntry.COLUMN_NAME_TITLE,
                parameterName
        };
        Cursor cursor = null;
        int result = 0;
        try {
            cursor = db.query(
                    DataBaseConstants.APP_TABLE_NAME, // The table to query
                    projection,                       // The columns to return
                    selection,                        // The columns for the WHERE clause
                    selectionArgs,                    // The values for the WHERE clause
                    null,                             // don't group the rows
                    null,                             // don't filter by row groups
                    null                              // The sort order
            );
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    result = cursor.getInt(1);
                } while (cursor.moveToNext());
            }
        } catch (Throwable throwable) {
            Logger.e("Error getAppParameter_String", throwable);
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                    db.close();
                }  catch (Throwable throwable) {
                    Logger.e("Error close DB getAppParameter_String", throwable);
                }
            }
        }
        return result;
    }

    public static void setAppParameter(Context context, ContentValues contentValues) {
        DataBaseHelper mDbHelperGroups = new DataBaseHelper(context);
        SQLiteDatabase db;
        try {
            db = mDbHelperGroups.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e("Error open DB setAppParameter", e);
            return;
        }
        int count = DataBaseUtilities.rowsCount(db, DataBaseConstants.APP_TABLE_NAME);
        Logger.d("AppSetting rows count " + count);
        if (count == 0) {
            addAppParameter(db, contentValues);
        } else if (count == 1) {
            updateAppParameter(db, contentValues);
        }
    }

    private static void updateAppParameter(SQLiteDatabase db, ContentValues contentValues) {
        // Which row to update, based on the ID
        String selection = AppEntry.COLUMN_NAME_TITLE + "=?";
        String[] selectionArgs = { APP_SETTINGS_TABLE_NAME };
        try {
            int result = db.update(
                    DataBaseConstants.APP_TABLE_NAME,
                    contentValues,
                    selection,
                    selectionArgs);
            Logger.d("Update App parameter " + contentValues + " result " + result);
        } catch (Throwable throwable) {
            Logger.e("Error updateAppParameter", throwable);
        } finally {
            try {
                db.close();
            }  catch (Throwable throwable) {
                Logger.e("Error close DB updateAppParameter", throwable);
            }
        }
    }

    private static void addAppParameter(SQLiteDatabase db, ContentValues values) {
        // Insert the new row, returning the primary key value of the new row
        try {
            values.put(AppEntry.COLUMN_NAME_TITLE, APP_SETTINGS_TABLE_NAME);
            long result = db.insert(
                    DataBaseConstants.APP_TABLE_NAME,
                    AppEntry.COLUMN_NAME_NULLABLE,
                    values);
            Logger.d("Add App parameter " + values + " result " + result);
        } catch (Throwable throwable) {
            Logger.e("Error addAppParameter", throwable);
        }  finally {
            try {
                db.close();
            }  catch (Throwable throwable) {
                Logger.e("Error close DB addAppParameter", throwable);
            }
        }
    }
}