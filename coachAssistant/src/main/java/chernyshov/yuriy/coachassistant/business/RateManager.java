package chernyshov.yuriy.coachassistant.business;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.database.AppSettingsDataBaseManager;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.AppEntry;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 10/1/13
 * Time: 8:52 PM
 */
public class RateManager {

    private static final int DIALOG_COUNTER_MAX = 10;
    private static final int DIALOG_STOP_VALUE = -1;

    private boolean doProcess = true;
    private static RateManager instance;

    private RateManager() {
        super();
        instance = this;
        doProcess = true;
    }

    public static RateManager getInstance() {
        if (instance == null) {
            synchronized (RateManager.class) {
                if (instance == null) {
                    new RateManager();
                }
            }
        }
        return instance;
    }

    public void init() {
        int value = AppSettingsDataBaseManager.getAppParameter_int(MainApp.getInstance(),
                AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER);
        if (value == DIALOG_STOP_VALUE) {
            doProcess = false;
        }
    }

    public void incrementCounter(final Activity activity) {

        if (!doProcess) {
            return;
        }

        int value = AppSettingsDataBaseManager.getAppParameter_int(MainApp.getInstance(),
                AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER);

        Logger.d(getClass().getSimpleName() + " incrementCounter, from: " + value);
        value += 1;

        ContentValues values = new ContentValues();
        values.put(AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER, value);
        AppSettingsDataBaseManager.setAppParameter(MainApp.getInstance(), values);

        if (value >= DIALOG_COUNTER_MAX) {
            MainApp.getInstance().runInUIThread(new Runnable() {
                @Override
                public void run() {
                    showRateDialog(activity);
                }
            });
        }
    }

    private void dropCounter() {
        ContentValues values = new ContentValues();
        values.put(AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER, 0);
        AppSettingsDataBaseManager.setAppParameter(MainApp.getInstance(), values);

        doProcess = true;
    }

    private void disableCounter() {
        ContentValues values = new ContentValues();
        values.put(AppEntry.COLUMN_NAME_RATE_DIALOG_COUNTER, -1);
        AppSettingsDataBaseManager.setAppParameter(MainApp.getInstance(), values);

        doProcess = false;
    }

    private void rateApplication(Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + MainApp.getInstance().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);
        } catch (Throwable e) {
            Logger.e("Couldn't launch the market", e);
        }

        disableCounter();
    }

    private void showRateDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        // Get the layout inflater
        LayoutInflater inflater = activity.getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_rate_app_view, null);
        if (view != null) {
            builder.setView(view)
                    .setCancelable(false)
                    .setTitle(activity.getResources().getString(R.string.rate_dialog_title));
            final AlertDialog dialog = builder.create();
            dialog.show();

            FlurryManager.logEvent(FlurryManager.EVENT_ID_SHOW_RATE_DIALOG);

            Button rateBtn = (Button) view.findViewById(R.id.rate_dialog_rate_btn);
            if (rateBtn != null) {
                rateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rateApplication(activity);
                        dialog.dismiss();
                        FlurryManager.logEvent(FlurryManager.EVENT_ID_SHOW_RATE_DIALOG, "rate-dialog-result", "rate-now");
                    }
                });
            } else {
                Logger.e(getClass().getSimpleName() + " showRateDialog rateBtn is null");
            }
            Button laterBtn = (Button) view.findViewById(R.id.rate_dialog_later_btn);
            if (laterBtn != null) {
                laterBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dropCounter();
                        dialog.dismiss();
                        FlurryManager.logEvent(FlurryManager.EVENT_ID_SHOW_RATE_DIALOG, "rate-dialog-result", "later-later");
                    }
                });
            } else {
                Logger.e(getClass().getSimpleName() + " showRateDialog laterBtn is null");
            }
            Button noBtn = (Button) view.findViewById(R.id.rate_dialog_no_btn);
            if (noBtn != null) {
                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        disableCounter();
                        dialog.dismiss();
                        FlurryManager.logEvent(FlurryManager.EVENT_ID_SHOW_RATE_DIALOG, "rate-dialog-result", "rate-never");
                    }
                });
            } else {
                Logger.e(getClass().getSimpleName() + " showRateDialog noBtn is null");
            }

        } else {
            Logger.e(getClass().getSimpleName() + " showRateDialog view is null");
        }
    }
}