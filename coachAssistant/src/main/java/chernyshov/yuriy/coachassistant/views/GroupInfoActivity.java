package chernyshov.yuriy.coachassistant.views;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.database.GroupsDataBaseManager;
import chernyshov.yuriy.coachassistant.model.Group;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.BitmapUtilities;
import chernyshov.yuriy.coachassistant.utilities.IntentUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.vo.GroupEntry;
import chernyshov.yuriy.coachassistant.vo.RequestCodeVO;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 11.01.13
 * Time: 12:32
 */
public class GroupInfoActivity extends AppBaseActivity {

    private String groupName;
    private static Uri uploadPictureFileUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_info_layout);
        Intent intent = getIntent();
        Bundle bundle = null;
        if (intent != null) {
            bundle = intent.getExtras();
        }
        if (bundle == null) {
            Logger.w("GroupInfo bundle null");
            return;
        }
        groupName = bundle.getString(ExtrasVO.GROUP_NAME);
        TextView headerView = (TextView) findViewById(R.id.group_info_name_view);
        headerView.setText(groupName);

        Group group = getGroup(groupName);

        ImageView groupIcon = (ImageView) findViewById(R.id.group_info_img_view);
        groupIcon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                createGroupIconContextMenu();
                return false;
            }
        });
        if (group != null && !TextUtils.isEmpty(group.getImageURL())) {
            setGroupMainImage(group.getImageURL());
        }

        final TextView descriptionView = (TextView) findViewById(R.id.group_info_description_view);
        if (group != null && !TextUtils.isEmpty(group.getDescription())) {
            descriptionView.setText(group.getDescription());
        }
        descriptionView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (descriptionView != null) {
                    createDescriptionDialog(descriptionView, descriptionView.getText().toString().trim());
                }
                return false;
            }
        });
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
        //requestCode = (requestCode & 0xffff);
        if (resultCode == android.app.Activity.RESULT_OK) {
            String mediaFilePath = null;
            if (requestCode == RequestCodeVO.GROUP_INFO_IMG_CAMERA_RESULT) {
                Logger.d("GroupInfoActivity onActivityResult CAMERA result uri: " + uploadPictureFileUri);
                if (uploadPictureFileUri != null) {
                    mediaFilePath = AppUtilities.getMediaPath(uploadPictureFileUri);
                    uploadPictureFileUri = null;
                }
                Logger.d("GroupInfoActivity onActivityResult CAMERA result path: " + mediaFilePath);
                setGroupMainImage(mediaFilePath);
                updateImageInDB(mediaFilePath);
            }
            if (requestCode == RequestCodeVO.GROUP_INFO_IMG_GALLERY_RESULT) {
                Uri uri = data.getData();
                if (uri != null) {
                    mediaFilePath = AppUtilities.getMediaPath(uri);
                    setGroupMainImage(mediaFilePath);
                    updateImageInDB(mediaFilePath);
                }
            }
        }
    }

    private void createGroupIconContextMenu() {
        Logger.d("Context menu for group icon");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.main_icon_context_menu_header));
        builder.setItems(R.array.main_img_context_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent getGalleryImageIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        getGalleryImageIntent.setType("image/*");
                        getGalleryImageIntent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(getGalleryImageIntent, RequestCodeVO.GROUP_INFO_IMG_GALLERY_RESULT);
                        break;
                    case 1:
                        uploadPictureFileUri = IntentUtilities.createGetPictureIntent(GroupInfoActivity.this,
                                GroupInfoActivity.this, RequestCodeVO.GROUP_INFO_IMG_CAMERA_RESULT);
                        break;
                    case 2:
                        setGroupMainImage(null);
                        updateImageInDB("");
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setGroupMainImage(String mediaFilePath) {
        ImageView groupIcon = (ImageView) findViewById(R.id.group_info_img_view);
        if (groupIcon == null) {
            return;
        }
        if (!TextUtils.isEmpty(mediaFilePath)) {
            Bitmap bitmap = BitmapUtilities.decodeSampledBitmapFromFile(mediaFilePath,
                    (int) (128 * MainApp.RESDENSITY), (int) (128 * MainApp.RESDENSITY));
            if (bitmap != null) {
                groupIcon.setImageBitmap(bitmap);
            }
        } else {
            groupIcon.setImageDrawable(getResources().getDrawable(R.drawable.main_img_placeholder));
        }
    }

    private void updateImageInDB(String mediaFilePath) {
        ContentValues values = new ContentValues();
        values.put(GroupEntry.COLUMN_NAME_IMG_URL, mediaFilePath);
        GroupsDataBaseManager.setGroupParams(this, groupName, values);
    }

    private void updateDescriptionInDB(String description) {
        ContentValues values = new ContentValues();
        values.put(GroupEntry.COLUMN_NAME_DESCRIPTION, description);
        GroupsDataBaseManager.setGroupParams(this, groupName, values);
    }

    private Group getGroup(String groupName) {
        return GroupsDataBaseManager.getGroup(this, groupName);
    }

    private void createDescriptionDialog(final TextView descriptionView, String value) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        final EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
        String positiveButtonLabel;
        if (editText != null && !TextUtils.isEmpty(value) &&
                !TextUtils.equals(value, getString(R.string.group_info_description_textholder))) {
            editText.setText(value);
            positiveButtonLabel = getString(R.string.apply);
        } else {
            positiveButtonLabel = getString(R.string.add);
        }
        builder.setView(view)
                .setCancelable(false)
                .setTitle(getResources().getString(R.string.group_info_description_textholder))
                .setPositiveButton(positiveButtonLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (editText != null) {
                            String groupDescription = editText.getText().toString().trim();
                            updateDescriptionInDB(groupDescription);
                            if (descriptionView != null) {
                                descriptionView.setText(groupDescription);
                            }
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_string), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}