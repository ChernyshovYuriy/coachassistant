package chernyshov.yuriy.coachassistant.views;

import android.content.Intent;
import android.os.Bundle;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.database.PersonsDataBaseManager;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 12/13/12
 * Time: 11:07 PM
 */
public class GroupBaseActivity extends AppBaseActivity {

    protected ArrayList<Person> persons;
    protected String groupName;

    public String getHeaderString() {
        Intent intent = getIntent();
        Bundle bundle = null;
        if (intent != null) {
            bundle = intent.getExtras();
        }
        if (bundle != null) {
            groupName = bundle.getString(ExtrasVO.GROUP_NAME);
            String boldOpenTag = "<b>";
            String boldCloseTag = "</b>";
            String emptySpace = " ";

            StringBuilder builder = new StringBuilder();
            builder.append(getResources().getString(R.string.main_view_groups_spinner_desc_text));
            builder.append(emptySpace);
            builder.append(boldOpenTag);
            builder.append(groupName);
            builder.append(boldCloseTag);

            return builder.toString();
        } else {
            return "";
        }
    }

    public void setPersonPresence(Person person, boolean wasAtSession) {
        long currentDayTimestamp = AppUtilities.startOfDay(System.currentTimeMillis());
        boolean isRecordExist = false;
        Iterator<PersonSessionResultVO> personSessionResultVOIterator = person.getResults().iterator();
        while (personSessionResultVOIterator.hasNext()) {
            PersonSessionResultVO personSessionResultVO = personSessionResultVOIterator.next();
            if (personSessionResultVO != null) {
                if (personSessionResultVO.timestamp == currentDayTimestamp) {
                    personSessionResultVO.wasAtSession = wasAtSession;
                    isRecordExist = true;
                    break;
                }
            }
        }
        if (!isRecordExist) {
            PersonSessionResultVO vo = new PersonSessionResultVO();
            vo.timestamp = currentDayTimestamp;
            vo.wasAtSession = wasAtSession;
            person.getResults().add(vo);
        }
    }

    public void setPersonResult(Person person, byte result) {
        long currentDayTimestamp = AppUtilities.startOfDay(System.currentTimeMillis());
        boolean isRecordExist = false;
        Iterator<PersonSessionResultVO> personSessionResultVOIterator = person.getResults().iterator();
        while (personSessionResultVOIterator.hasNext()) {
            PersonSessionResultVO personSessionResultVO = personSessionResultVOIterator.next();
            if (personSessionResultVO != null) {
                if (personSessionResultVO.timestamp == currentDayTimestamp) {
                    personSessionResultVO.result = result;
                    isRecordExist = true;
                    break;
                }
            }
        }
        if (!isRecordExist) {
            PersonSessionResultVO vo = new PersonSessionResultVO();
            vo.timestamp = currentDayTimestamp;
            vo.result = result;
            person.getResults().add(vo);
        }
    }

    protected ArrayList<Person> loadPersons() {
        this.persons = new ArrayList<Person>();
        ArrayList<Person> persons = PersonsDataBaseManager.getPersons(groupName, this);
        this.persons.clear();
        if (persons != null && persons.size() > 0) {
            for (Person person : persons) {
                this.persons.add(person);
            }
        }
        return this.persons;
    }
}