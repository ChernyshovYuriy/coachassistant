package chernyshov.yuriy.coachassistant.views;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import chernyshov.yuriy.coachassistant.BuildConfig;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.ContactsQuery;
import chernyshov.yuriy.coachassistant.business.RateManager;
import chernyshov.yuriy.coachassistant.business.database.GroupsDataBaseManager;
import chernyshov.yuriy.coachassistant.business.database.PersonsDataBaseManager;
import chernyshov.yuriy.coachassistant.model.ImportedPerson;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.ImageLoader;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.views.adapters.ContactsAdapter;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.vo.PersonEntry;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class ContactsFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    private final static String ARG_POSITION = "position";
    private int mCurrentPosition = -1;

    // Stores the previously selected search item so that on a configuration change the same item
    // can be reselected again
    private int mPreviouslySelectedSearchItem = 0;
    // Whether or not the search query has changed since the last time the loader was refreshed
    private boolean mSearchQueryChanged;
    // Stores the current search query term
    private String mSearchTerm;
    private ContactsAdapter mAdapter; // The main query adapter
    private ImageLoader mImageLoader; // Handles loading the contact image in a background thread

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         /*
         * An ImageLoader object loads and resizes an image in the background and binds it to the
         * QuickContactBadge in each item layout of the ListView. ImageLoader implements memory
         * caching for each image, which substantially improves refreshes of the ListView as the
         * user scrolls through it.
         *
         * To learn more about downloading images asynchronously and caching the results, read the
         * Android training class Displaying Bitmaps Efficiently.
         *
         * http://developer.android.com/training/displaying-bitmaps/
         */
        mImageLoader = new ImageLoader(getActivity(), getListPreferredItemHeight()) {
            @Override
            protected Bitmap processBitmap(Object data) {
                // This gets called in a background thread and passed the data from
                // ImageLoader.loadImage().
                return loadContactPhotoThumbnail((String) data, getImageSize());
            }
        };

        // Set a placeholder loading image for the image loader
        mImageLoader.setLoadingImage(R.drawable.ic_social_person);

        // Add a cache to the image loader
        mImageLoader.addImageCache(getActivity().getSupportFragmentManager(), 0.1f);

        // Create the main contacts adapter
        mAdapter = new ContactsAdapter(getActivity(), mImageLoader);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // If activity recreated (such as from screen rotate), restore
        // the previous article selection set by onSaveInstanceState().
        // This is primarily necessary when in the two-pane layout.
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.contacts_list_layout, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            updateArticleView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            // Set article based on saved instance state defined during onCreateView
            updateArticleView(mCurrentPosition);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up ListView, assign adapter and set some listeners. The adapter was previously created in onCreate().
        setListAdapter(mAdapter);

        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                // Pause image loader to ensure smoother scrolling when flinging
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    mImageLoader.setPauseWork(true);
                } else {
                    mImageLoader.setPauseWork(false);
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {}
        });

        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Logger.d(getClass().getSimpleName() + " on item long click " + view + " " + position + " " + id);
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                if (cursor != null) {
                    createItemContextMenu(cursor);
                }
                return false;
            }
        });

        // If there's a previously selected search item from a saved state then don't bother
        // initializing the loader as it will be restarted later when the query is populated into
        // the action bar search view (see onQueryTextChange() in onCreateOptionsMenu()).
        if (mPreviouslySelectedSearchItem == 0) {
            // Initialize the loader, and create a loader identified by ContactsQuery.QUERY_ID
            getLoaderManager().initLoader(ContactsQuery.QUERY_ID, null, this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // In the case onPause() is called during a fling the image loader is
        // un-paused to let any remaining background work complete.
        mImageLoader.setPauseWork(false);

        if (getLoaderManager().getLoader(ContactsQuery.QUERY_ID) != null) {
            getLoaderManager().destroyLoader(ContactsQuery.QUERY_ID);
        }

        if (getLoaderManager().getLoader(ContactsQuery.PHONE_QUERY_ID) != null) {
            getLoaderManager().destroyLoader(ContactsQuery.PHONE_QUERY_ID);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // If this is the loader for finding contacts in the Contacts Provider
        // (the only one supported)
        Logger.d(getClass().getSimpleName() + " on create loader, id: " + i);

        Uri contentUri;
        if (i == ContactsQuery.PHONE_QUERY_ID) {
            if (bundle != null) {
                contentUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

                return new CursorLoader(getActivity(),
                        contentUri,
                        null,
                        ContactsQuery.SELECTION_PHONE + bundle.getString(ExtrasVO.CONTACT_ID),
                        null,
                        ContactsQuery.SORT_ORDER);
            }
        }
        if (i == ContactsQuery.QUERY_ID) {

            // There are two types of searches, one which displays all contacts and
            // one which filters contacts by a search query. If mSearchTerm is set
            // then a search query has been entered and the latter should be used.

            if (mSearchTerm == null) {
                // Since there's no search string, use the content URI that searches the entire
                // Contacts table
                contentUri = ContactsQuery.CONTENT_URI;
            } else {
                // Since there's a search string, use the special content Uri that searches the
                // Contacts table. The URI consists of a base Uri and the search string.
                contentUri = Uri.withAppendedPath(ContactsQuery.FILTER_URI, Uri.encode(mSearchTerm));
            }

            // Returns a new CursorLoader for querying the Contacts table. No arguments are used
            // for the selection clause. The search string is either encoded onto the content URI,
            // or no contacts search string is used. The other search criteria are constants. See
            // the ContactsQuery interface.
            return new CursorLoader(getActivity(),
                    contentUri,
                    ContactsQuery.PROJECTION,
                    ContactsQuery.SELECTION,
                    null,
                    ContactsQuery.SORT_ORDER);
        }
        Logger.w(getClass().getSimpleName() + " onCreateLoader - incorrect ID provided (" + i + ")");
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        // This swaps the new cursor into the adapter.

        Logger.d(getClass().getSimpleName() + " onLoadFinished, id: " + cursorLoader.getId());

        if (cursorLoader.getId() == ContactsQuery.PHONE_QUERY_ID) {
            String displayName = "";
            String number = "";
            String photoThumbData = "";
            while (cursor.moveToNext()) {
                number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                photoThumbData = cursor.getString(ContactsQuery.PHOTO_THUMBNAIL_DATA);
                int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                switch (type) {
                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        Logger.i("Home Phone: " + number);
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        Logger.i("Mobile Phone: " + number);
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        Logger.i("Work Phone: " + number);
                        break;
                }
            }

            cursor.close();

            if (displayName != null && !displayName.equals("")) {
                ImportedPerson importedPerson = new ImportedPerson();
                importedPerson.setDisplayName(displayName);
                importedPerson.setPhoneNumber(number);
                importedPerson.setContactThumbUri(photoThumbData);

                showGroupSelectionDialog(importedPerson);
            }
        }

        if (cursorLoader.getId() == ContactsQuery.QUERY_ID) {

            mAdapter.swapCursor(cursor);

            // If this is a two-pane layout and there is a search query then
            // there is some additional work to do around default selected
            // search item.
            /*if (!TextUtils.isEmpty(mSearchTerm) && mSearchQueryChanged) {
                // Selects the first item in results, unless this fragment has
                // been restored from a saved state (like orientation change)
                // in which case it selects the previously selected search item.
                if (cursor != null && cursor.moveToPosition(mPreviouslySelectedSearchItem)) {
                    // Creates the content Uri for the previously selected contact by appending the
                    // contact's ID to the Contacts table content Uri
                    final Uri uri = Uri.withAppendedPath(
                            ContactsContract.Contacts.CONTENT_URI, String.valueOf(cursor.getLong(ContactsQuery.ID)));
                    //mOnContactSelectedListener.onContactSelected(uri);
                    //getListView().setItemChecked(mPreviouslySelectedSearchItem, true);
                } else {
                    // No results, clear selection.
                    //onSelectionCleared();
                }
                // Only restore from saved state one time. Next time fall back
                // to selecting first item. If the fragment state is saved again
                // then the currently selected item will once again be saved.
                //mPreviouslySelectedSearchItem = 0;
                //mSearchQueryChanged = false;
            }*/
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        if (cursorLoader.getId() == ContactsQuery.QUERY_ID) {
            // When the loader is being reset, clear the cursor from the adapter. This allows the
            // cursor resources to be freed.
            mAdapter.swapCursor(null);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(getClass().getSimpleName() + " onItemClick");
    }

    public void updateArticleView(int position) {
        mCurrentPosition = position;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }

    private void createItemContextMenu(final Cursor cursor) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String contactDisplayName = cursor.getString(ContactsQuery.DISPLAY_NAME);
        builder.setTitle(contactDisplayName);
        builder.setItems(R.array.contact_context_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        String hasPhoneNumber = cursor.getString(ContactsQuery.HAS_PHONE_NUMBER);
                        if (hasPhoneNumber != null && hasPhoneNumber.equals("1")) {
                            Bundle bundle = new Bundle();
                            bundle.putString(ExtrasVO.CONTACT_ID, cursor.getString(ContactsQuery.ID));
                            if (getLoaderManager().getLoader(ContactsQuery.PHONE_QUERY_ID) == null) {
                                // Initialize the loader, and create a loader identified by ContactsQuery.PHONE_QUERY_ID
                                getLoaderManager().initLoader(ContactsQuery.PHONE_QUERY_ID, bundle, ContactsFragment.this);
                            } else {
                                // Restart the loader identified by ContactsQuery.PHONE_QUERY_ID
                                getLoaderManager().restartLoader(ContactsQuery.PHONE_QUERY_ID, bundle, ContactsFragment.this);
                            }
                        } else {
                            Logger.i(getClass().getSimpleName() + " Contact has no Phone numbers");
                            ImportedPerson importedPerson = new ImportedPerson();
                            importedPerson.setDisplayName(contactDisplayName);
                            importedPerson.setContactThumbUri(cursor.getString(ContactsQuery.PHOTO_THUMBNAIL_DATA));
                            showGroupSelectionDialog(importedPerson);
                        }
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showGroupSelectionDialog(final ImportedPerson importedPerson) {
        final boolean[] doRate = {false};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.group_selection_title);
        final ArrayList<String> groupsList = GroupsDataBaseManager.readGroups(getActivity());
        @SuppressWarnings("ConstantConditions")
        final CharSequence[] groups = new CharSequence[groupsList.size()];
        int counter = 0;
        for (String group : groupsList) {
            groups[counter++] = group;
        }
        builder.setItems(groups, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int personHash = AppUtilities.getMD5fromString(importedPerson.getDisplayName() +
                        groups[which].toString()).hashCode();
                Person person = PersonsDataBaseManager.getPerson(personHash, getActivity());
                if (person == null) {
                    person = PersonsDataBaseManager.addPerson(importedPerson.getDisplayName(),
                            groups[which].toString(), getActivity());

                    if (person != null) {
                        ContentValues values = new ContentValues();
                        values.put(PersonEntry.COLUMN_NAME_TITLE, importedPerson.getDisplayName());
                        values.put(PersonEntry.COLUMN_NAME_PHONE, importedPerson.getPhoneNumber());

                        Uri thumbUri;
                        // If Android 3.0 or later, converts the Uri passed as a string to a Uri object.
                        if (AppUtilities.hasHoneycomb()) {
                            thumbUri = Uri.parse(importedPerson.getContactThumbUri());
                        } else {
                            // For versions prior to Android 3.0, appends the string argument to the content
                            // Uri for the Contacts table.
                            final Uri contactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, importedPerson.getContactThumbUri());

                            // Appends the content Uri for the Contacts.Photo table to the previously
                            // constructed contact Uri to yield a content URI for the thumbnail image
                            thumbUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
                        }

                        values.put(PersonEntry.COLUMN_NAME_CONTACT_THUMB_URI, thumbUri.toString());

                        PersonsDataBaseManager.updatePersonDetails(person.getDisplayName(), getActivity(), values);

                        doRate[0] = true;
                    }
                    ((GroupViewActivity) getActivity()).updateList();
                } else {
                    SafeToast.showToastAnyThread(getString(R.string.contact_exists_message));
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (doRate[0]) {
                    RateManager.getInstance().incrementCounter(getActivity());
                }
            }
        });
        dialog.show();
    }

    /*private String parseUri(Uri uri) {
        String fileName = "unknown";//default fileName
        Uri filePathUri = uri;
        if (uri.getScheme().compareTo("content") == 0) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor.moveToFirst()) {
                //Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                int columnIndex;
                boolean isError = true;
                for (String name: cursor.getColumnNames()) {
                    columnIndex = cursor.getColumnIndex(name);
                    Logger.d("TRACE index: " + columnIndex + ", name: " + name);
                    try {
                        Logger.d("TRACE value(str): " + cursor.getString(columnIndex));
                    } catch (Throwable throwable) {
                        isError = false;
                    }
                    if (!isError) {
                        try {
                            Logger.d("TRACE value(blob): " + Arrays.toString(cursor.getBlob(columnIndex)));
                        } catch (Throwable throwable) {
                            isError = false;
                        }
                    }
                }
                //int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                //filePathUri = Uri.parse(cursor.getString(column_index));
                //fileName = filePathUri.getLastPathSegment();
            }
        } else if (uri.getScheme().compareTo("file") == 0) {
            fileName = filePathUri.getLastPathSegment();
        } else {
            fileName = fileName + "_" + filePathUri.getLastPathSegment();
        }
        return fileName;
    }*/

    /**
     * Gets the preferred height for each item in the ListView, in pixels, after accounting for
     * screen density. ImageLoader uses this value to resize thumbnail images to match the ListView
     * item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    private int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getActivity().getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new android.util.DisplayMetrics();

        // Populate the DisplayMetrics
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }

    /**
     * Decodes and scales a contact's image from a file pointed to by a Uri in the contact's data,
     * and returns the result as a Bitmap. The column that contains the Uri varies according to the
     * platform version.
     *
     * @param photoData For platforms prior to Android 3.0, provide the Contact._ID column value.
     *                  For Android 3.0 and later, provide the Contact.PHOTO_THUMBNAIL_URI value.
     * @param imageSize The desired target width and height of the output image in pixels.
     * @return A Bitmap containing the contact's image, resized to fit the provided image size. If
     * no thumbnail exists, returns null.
     */
    private Bitmap loadContactPhotoThumbnail(String photoData, int imageSize) {

        // Ensures the Fragment is still added to an activity. As this method is called in a
        // background thread, there's the possibility the Fragment is no longer attached and
        // added to an activity. If so, no need to spend resources loading the contact photo.
        if (!isAdded() || getActivity() == null) {
            return null;
        }

        // Instantiates an AssetFileDescriptor. Given a content Uri pointing to an image file, the
        // ContentResolver can return an AssetFileDescriptor for the file.
        AssetFileDescriptor assetFileDescriptor = null;

        // This "try" block catches an Exception if the file descriptor returned from the Contacts
        // Provider doesn't point to an existing file.
        try {
            Uri thumbUri;
            // If Android 3.0 or later, converts the Uri passed as a string to a Uri object.
            if (AppUtilities.hasHoneycomb()) {
                thumbUri = Uri.parse(photoData);
            } else {
                // For versions prior to Android 3.0, appends the string argument to the content
                // Uri for the Contacts table.
                final Uri contactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, photoData);

                // Appends the content Uri for the Contacts.Photo table to the previously
                // constructed contact Uri to yield a content URI for the thumbnail image
                thumbUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
            }
            // Retrieves a file descriptor from the Contacts Provider. To learn more about this
            // feature, read the reference documentation for
            // ContentResolver#openAssetFileDescriptor.
            assetFileDescriptor = getActivity().getContentResolver().openAssetFileDescriptor(thumbUri, "r");

            // Gets a FileDescriptor from the AssetFileDescriptor. A BitmapFactory object can
            // decode the contents of a file pointed to by a FileDescriptor into a Bitmap.
            FileDescriptor fileDescriptor = assetFileDescriptor.getFileDescriptor();

            if (fileDescriptor != null) {
                // Decodes a Bitmap from the image pointed to by the FileDescriptor, and scales it
                // to the specified width and height
                return ImageLoader.decodeSampledBitmapFromDescriptor(fileDescriptor, imageSize, imageSize);
            }
        } catch (FileNotFoundException e) {
            // If the file pointed to by the thumbnail URI doesn't exist, or the file can't be
            // opened in "read" mode, ContentResolver.openAssetFileDescriptor throws a
            // FileNotFoundException.
            if (BuildConfig.DEBUG) {
                Logger.e("Contact photo thumbnail not found for contact " + photoData + ": " + e.toString());
            }
        } finally {
            // If an AssetFileDescriptor was returned, try to close it
            if (assetFileDescriptor != null) {
                try {
                    assetFileDescriptor.close();
                } catch (IOException e) {
                    // Closing a file descriptor might cause an IOException if the file is
                    // already closed. Nothing extra is needed to handle this.
                }
            }
        }

        // If the decoding failed, returns null
        return null;
    }
}