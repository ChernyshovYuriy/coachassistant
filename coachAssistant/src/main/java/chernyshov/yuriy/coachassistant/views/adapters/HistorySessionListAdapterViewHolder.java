package chernyshov.yuriy.coachassistant.views.adapters;

import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/26/12
 * Time: 3:41 PM
 */
public class HistorySessionListAdapterViewHolder {
    TextView displayNameView;
    TextView pointsView;
    CheckBox presentCheckView;
}