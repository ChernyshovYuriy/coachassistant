package chernyshov.yuriy.coachassistant.views;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.business.FlurryManager;
import chernyshov.yuriy.coachassistant.utilities.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 11.01.13
 * Time: 17:53
 */
public abstract class AppBaseActivity extends ActionBarActivity {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Logger.d("+ " + this.getClass().getSimpleName() + " onCreate");
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.d("+ " + this.getClass().getSimpleName() + " onStart");
        FlurryManager.startOrContinueFlurrySession(this, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d("+ " + this.getClass().getSimpleName() + " onResume");

        MainApp.getInstance().setActiveActivity(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.d("+ " + this.getClass().getSimpleName() + " onStop");
        FlurryManager.endFlurrySession(this);
    }
}