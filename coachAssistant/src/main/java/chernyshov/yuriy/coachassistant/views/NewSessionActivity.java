package chernyshov.yuriy.coachassistant.views;

import android.content.ContentValues;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Iterator;

import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.database.PersonsDataBaseManager;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.views.adapters.NewSessionListAdapter;
import chernyshov.yuriy.coachassistant.views.adapters.NewSessionListAdapterData;
import chernyshov.yuriy.coachassistant.vo.PersonEntry;
import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 12/9/12
 * Time: 8:49 PM
 */
public class NewSessionActivity extends GroupBaseActivity {

    private final NewSessionListAdapterData mNewSessionListAdapterData = new NewSessionListAdapterData();
    private final NewSessionListAdapter mNewSessionListAdapter = new NewSessionListAdapter(mNewSessionListAdapterData, this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_new_session_layout);

        TextView headerView = (TextView) findViewById(R.id.group_new_session_header);
        if (headerView != null) {
            headerView.setText(Html.fromHtml(getHeaderString()));
        }

        TextView dateText = (TextView) findViewById(R.id.group_new_session_date);
        if (dateText != null) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            StringBuilder dateString = new StringBuilder();
            dateString.append(day).append("-").append(month + 1).append("-").append(year);
            dateText.setText(dateString.toString());
        }

        ListView listView = (ListView) findViewById(R.id.group_new_session_list);
        listView.setAdapter(mNewSessionListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mNewSessionListAdapterData.fillItems(loadPersons());
        mNewSessionListAdapter.notifyDataSetChanged();

        long currentDayTimestamp = AppUtilities.startOfDay(System.currentTimeMillis());
        Iterator<Person> iterator = mNewSessionListAdapterData.itemsIterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (person != null) {
                Iterator<PersonSessionResultVO> personSessionResultVOIterator = person.getResults().iterator();
                boolean isSessionExists = false;
                while (personSessionResultVOIterator.hasNext()) {
                    PersonSessionResultVO personSession = personSessionResultVOIterator.next();
                    if (personSession != null && personSession.timestamp == currentDayTimestamp) {
                        SafeToast.showToastAnyThread(getString(R.string.session_already_exists));
                        isSessionExists = true;
                        break;
                    }
                }
                if (isSessionExists) {
                    break;
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // TODO: Show progress bar here
            saveData();
            //return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void saveData() {
        Logger.d("Save Session Data");
        Iterator<Person> iterator = mNewSessionListAdapterData.itemsIterator();
        int i = 0;
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (person != null) {
                if (person.getResults() != null) {
                    byte[] bytes = AppUtilities.arrayListToByteArray(person.getResults());
                    Logger.d("NewSessionActivity setPersonResult bytes: "
                            + (bytes != null ? bytes.length : 0));

                    ContentValues values = new ContentValues();
                    values.put(PersonEntry.COLUMN_NAME_TITLE, person.getDisplayName());
                    values.put(PersonEntry.COLUMN_NAME_RESULT, bytes);
                    values.put(PersonEntry.COLUMN_NAME_GROUP, groupName);

                    PersonsDataBaseManager.updatePersonDetails(person.getDisplayName(), this, values);
                }  else {
                    Logger.d("NewSessionActivity setPersonResult bytes null");
                }
                i++;
            }
        }
    }
}