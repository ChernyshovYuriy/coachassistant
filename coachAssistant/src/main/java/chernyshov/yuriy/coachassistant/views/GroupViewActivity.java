package chernyshov.yuriy.coachassistant.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.FlurryManager;
import chernyshov.yuriy.coachassistant.business.RateManager;
import chernyshov.yuriy.coachassistant.business.database.GroupsDataBaseManager;
import chernyshov.yuriy.coachassistant.business.database.PersonsDataBaseManager;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.BitmapUtilities;
import chernyshov.yuriy.coachassistant.utilities.ImageLoader;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.views.adapters.GroupListAdapter;
import chernyshov.yuriy.coachassistant.views.adapters.GroupListAdapterData;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 02.12.12
 * Time: 14:57
 */
public class GroupViewActivity extends GroupBaseActivity {

    private Spinner groupMenuSpinner;
    private static final String CONTACT_FRAGMENT_TAG = "CONTACT_FRAGMENT_TAG";
    private final GroupListAdapterData mGroupListAdapterData = new GroupListAdapterData();
    private final GroupListAdapter mGroupListAdapter = new GroupListAdapter(mGroupListAdapterData, this);
    private ImageLoader mImageLoader; // Handles loading the contact image in a background thread

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_view_layout);

        TextView headerView = (TextView) findViewById(R.id.group_view_header);
        if (headerView != null) {
            headerView.setText(Html.fromHtml(getHeaderString()));
        }

        /*
         * An ImageLoader object loads and resizes an image in the background and binds it to the
         * QuickContactBadge in each item layout of the ListView. ImageLoader implements memory
         * caching for each image, which substantially improves refreshes of the ListView as the
         * user scrolls through it.
         *
         * To learn more about downloading images asynchronously and caching the results, read the
         * Android training class Displaying Bitmaps Efficiently.
         *
         * http://developer.android.com/training/displaying-bitmaps/
         */
        mImageLoader = new ImageLoader(this, getListPreferredItemHeight()) {
            @Override
            protected Bitmap processBitmap(Object data) {
                // This gets called in a background thread and passed the data from ImageLoader.loadImage().
                if (data instanceof String) {
                    if (data.toString().startsWith("content:")) {
                        // Instantiates an AssetFileDescriptor. Given a content Uri pointing to an image file, the
                        // ContentResolver can return an AssetFileDescriptor for the file.
                        AssetFileDescriptor assetFileDescriptor = null;
                        // This "try" block catches an Exception if the file descriptor returned from the Contacts
                        // Provider doesn't point to an existing file.
                        try {

                            // Retrieves a file descriptor from the Contacts Provider. To learn more about this
                            // feature, read the reference documentation for
                            // ContentResolver#openAssetFileDescriptor.
                            assetFileDescriptor = getContentResolver().openAssetFileDescriptor(Uri.parse(data.toString()), "r");

                            // Gets a FileDescriptor from the AssetFileDescriptor. A BitmapFactory object can
                            // decode the contents of a file pointed to by a FileDescriptor into a Bitmap.
                            FileDescriptor fileDescriptor = assetFileDescriptor.getFileDescriptor();

                            if (fileDescriptor != null) {
                                // Decodes a Bitmap from the image pointed to by the FileDescriptor, and scales it
                                // to the specified width and height
                                return ImageLoader.decodeSampledBitmapFromDescriptor(fileDescriptor,
                                        (int) (48 * MainApp.RESDENSITY), (int) (48 * MainApp.RESDENSITY));
                            }
                        } catch (FileNotFoundException e) {
                            // If the file pointed to by the thumbnail URI doesn't exist, or the file can't be
                            // opened in "read" mode, ContentResolver.openAssetFileDescriptor throws a
                            // FileNotFoundException.
                            Logger.e("Contact photo thumbnail not found for contact " + data.toString() + ": "
                                    + e.toString());
                        } finally {
                            // If an AssetFileDescriptor was returned, try to close it
                            if (assetFileDescriptor != null) {
                                try {
                                    assetFileDescriptor.close();
                                } catch (IOException e) {
                                    // Closing a file descriptor might cause an IOException if the file is
                                    // already closed. Nothing extra is needed to handle this.
                                }
                            }
                        }
                    } else {
                        return BitmapUtilities.decodeSampledBitmapFromFile((String) data,
                                (int) (48 * MainApp.RESDENSITY), (int) (48 * MainApp.RESDENSITY));
                    }
                }
                return mImageLoader.getLoadingImage();
            }
        };

        // Set a placeholder loading image for the image loader
        mImageLoader.setLoadingImage(R.drawable.ic_social_person);

        // Add a cache to the image loader
        mImageLoader.addImageCache(getSupportFragmentManager(), 0.1f);

        //mGroupListAdapter.initFetcher(this);
        mGroupListAdapter.setImageLoader(mImageLoader);

        ListView listView = (ListView) findViewById(R.id.group_view_list);
        listView.setAdapter(mGroupListAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                createItemContextMenu(position, id);
                return false;
            }
        });

        groupMenuSpinner = (Spinner) findViewById(R.id.group_view_menu_spinner);
        if (groupMenuSpinner != null) {
            final ArrayAdapter<String> groupMenuAdapter
                    = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                    getResources().getStringArray(R.array.group_menu));
            groupMenuAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            groupMenuSpinner.setAdapter(groupMenuAdapter);
            groupMenuSpinner.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    switch (position) {
                        case 0:

                            break;
                        case 1:
                            FlurryManager.logEvent(
                                    FlurryManager.EVENT_ID_SESSION_SELECTED, "persons-count",
                                    String.valueOf(mGroupListAdapter.getCount())
                            );

                            Intent intent = new Intent(GroupViewActivity.this,
                                    NewSessionActivity.class);
                            intent.putExtra(ExtrasVO.GROUP_NAME, groupName);
                            startActivity(intent);
                            break;
                        case 2:
                            showSelectDateDialog();
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
    public void onPause() {
        super.onPause();

        // In the case onPause() is called during a fling the image loader is
        // un-paused to let any remaining background work complete.
        mImageLoader.setPauseWork(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Logger.d("GroupViewActivity options menu click '" + item.getTitle() + "'");
        switch (item.getItemId()) {
            case android.R.id.home:
                // TODO handle clicking the app icon / logo
                return false;
            case R.id.add_person:
                createNewPersonSelectionDialog();
                return false;
            case R.id.delete_group:
                showDeleteGroupDialog();
                return false;
            case R.id.about_group:
                Intent intent = new Intent(this, GroupInfoActivity.class);
                intent.putExtra(ExtrasVO.GROUP_NAME, groupName);
                startActivity(intent);
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Gets the preferred height for each item in the ListView, in pixels, after accounting for
     * screen density. ImageLoader uses this value to resize thumbnail images to match the ListView
     * item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    private int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new android.util.DisplayMetrics();

        // Populate the DisplayMetrics
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }

    private void createItemContextMenu(final int position, final long id) {
        Logger.e("Group item context menu for position: " + position + " id: " + id);
        Person person = persons.get(position);
        if (person != null) {
            Logger.d("Group item context menu for '" + person.getDisplayName() + "'");
        } else {
            Logger.e("Group item context menu for null");
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(person.getDisplayName());
        builder.setItems(R.array.person_context_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        FlurryManager.logEvent(FlurryManager.EVENT_ID_PERSON_INFO_SELECTED);
                        Logger.d(" - Info");
                        try {
                            final Person person = mGroupListAdapterData.getItem((int) id);
                            startActivity(PersonInfoActivity.makeStartActivityIntent(
                                    GroupViewActivity.this,
                                    AppUtilities.getMD5fromString(
                                            person.getDisplayName() + groupName
                                    ).hashCode()
                            ));
                        } catch (Throwable throwable) {
                            Logger.e("Get Info from Person", throwable);
                            SafeToast.showToastAnyThread(getString(R.string.create_intent_error));
                        }
                        break;
                    case 1:
                        Logger.d(" - Delete");
                        showDeleteUserDialog(position);
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateList() {
        if (groupMenuSpinner != null) {
            groupMenuSpinner.setSelection(0);
        }
        mGroupListAdapterData.fillItems(loadPersons());
        mGroupListAdapter.notifyDataSetChanged();

        //ListView listView = (ListView) findViewById(R.id.group_view_list);
        //listView.invalidate();
    }
    
    private void showDeleteUserDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_person_description_string)
                .setPositiveButton(R.string.delete_person_ok_string, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String personNameToDelete = persons.get(position).getDisplayName();
                        int result = PersonsDataBaseManager.deletePerson(personNameToDelete, GroupViewActivity.this);
                        if (result != -1) {
                            persons.remove(position);
                            updateList();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel_string, null);
        builder.create().show();
    }

    private void showDeleteGroupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_group_description_string)
                .setPositiveButton(R.string.delete_person_ok_string, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int result = GroupsDataBaseManager.deleteGroup(groupName, GroupViewActivity.this);
                        if (result != -1) {
                            finish();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel_string, null);
        builder.create().show();
    }

    private void createNewPersonSelectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.new_person_label));
        builder.setItems(R.array.add_person_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 1) {
                    createNewPersonDialog();
                } else {
                    showImportContactsFragment();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showImportContactsFragment() {
        if (findViewById(R.id.contacts_fragment) != null) {
            final ContactsFragment contactsFragment
                    = (ContactsFragment) getSupportFragmentManager()
                    .findFragmentByTag(CONTACT_FRAGMENT_TAG);

            if (contactsFragment != null) {
                return;
            }

            // If the frag is not available, we're in the one-pane layout and must swap frags...

            // Create fragment and give it an argument for the selected article
            final ContactsFragment newContactsFragment = new ContactsFragment();
            //Bundle args = new Bundle();
            //args.putInt(ContactsViewFragment.ARG_POSITION, position);
            //newContactsFragment.setArguments(args);
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the contacts_fragment view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.contacts_fragment, newContactsFragment, CONTACT_FRAGMENT_TAG);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }
    }

    private void createNewPersonDialog() {
        final boolean[] doRate = {false};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        builder.setView(view)
                .setCancelable(false)
                .setTitle(getResources().getString(R.string.new_person_label))
                .setPositiveButton(getString(R.string.new_group_ok_string), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
                        if (editText != null) {
                            Person person = PersonsDataBaseManager.addPerson(editText.getText().toString(), groupName,
                                    GroupViewActivity.this);
                            if (person != null) {
                                updateList();
                                doRate[0] = true;
                            } else {
                                createNewPersonSelectionDialog();
                            }
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_string), null);
        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (doRate[0]) {
                    RateManager.getInstance().incrementCounter(GroupViewActivity.this);
                }
            }
        });
        dialog.show();
    }

    private void showSelectDateDialog() {
        final TreeSet<String> timestamps = new TreeSet<String>();
        final HashMap<String, Long> hashMap = new HashMap<String, Long>();
        Iterator<Person> iterator = mGroupListAdapterData.itemsIterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (person != null) {
                Iterator<PersonSessionResultVO> personSessionResultVOIterator = person.getResults().iterator();
                while (personSessionResultVOIterator.hasNext()) {
                    PersonSessionResultVO personSession = personSessionResultVOIterator.next();
                    if (personSession != null) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(personSession.timestamp);
                        //String string = calendar.get(Calendar.DAY_OF_MONTH) + " " +
                        // calendar.getDisplayName(Calendar.MONTH, java.util.Calendar.LONG, Locale.getDefault()) + " " +
                        //        calendar.get(Calendar.YEAR);
                        String string = calendar.get(Calendar.DAY_OF_MONTH) + " " +
                                getMonthName(calendar.get(Calendar.MONTH)) + " " +
                        calendar.get(Calendar.YEAR);
                        timestamps.add(string);
                        hashMap.put(string, personSession.timestamp);
                    }
                }
            }
        }

        final CharSequence[] charSequences = timestamps.toArray(new CharSequence[timestamps.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.new_date_picker_label));
        builder.setItems(charSequences, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (groupMenuSpinner != null) {
                    groupMenuSpinner.setSelection(0);
                }

                FlurryManager.logEvent(FlurryManager.EVENT_ID_HISTORY_SELECTED, "history-records-count",
                        String.valueOf(charSequences.length));

                String string = timestamps.toArray()[which].toString();
                Intent intent = new Intent(GroupViewActivity.this, HistorySessionActivity.class);
                intent.putExtra(ExtrasVO.GROUP_NAME, groupName);
                intent.putExtra(ExtrasVO.HISTORY_TIMESTAMP_STRING, string);
                intent.putExtra(ExtrasVO.HISTORY_TIMESTAMP, hashMap.get(string));
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (groupMenuSpinner != null) {
                    groupMenuSpinner.setSelection(0);
                }
            }
        });
        dialog.show();
    }

    private String getMonthName(int month) {
         return getResources().getTextArray(R.array.month_names)[month].toString();
    }
}