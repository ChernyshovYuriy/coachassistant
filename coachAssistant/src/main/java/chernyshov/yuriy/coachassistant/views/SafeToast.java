package chernyshov.yuriy.coachassistant.views;

import android.content.Context;
import android.widget.Toast;
import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 06.11.12
 * Time: 15:57
 */
public class SafeToast {

    public static void showToastAnyThread(CharSequence text) {
        showToastAnyThread(MainApp.getInstance(), text);
    }

    public static void showToastAnyThread(final Context context, final CharSequence text) {
        if (AppUtilities.isRunningUIThread()) {
            // we are already in UI thread, it's safe to show Toast
            showToastUIThread(context, text);
        } else {
            // we are NOT in UI thread, so scheduling task in handler
            MainApp.getInstance().runInUIThread(new Runnable() {
                @Override
                public void run() {
                    showToastUIThread(context, text);
                }
            });
        }
    }

    private static void showToastUIThread(Context context, CharSequence text) {
        if (context == null) {
            context = MainApp.getInstance();
        }
        Logger.d("- Show toast: " + text);
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }
}