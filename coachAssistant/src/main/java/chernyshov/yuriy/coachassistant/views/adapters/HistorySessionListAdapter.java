package chernyshov.yuriy.coachassistant.views.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapter;
import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapterData;
import chernyshov.yuriy.coachassistant.views.GroupBaseActivity;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/26/12
 * Time: 3:42 PM
 */
public class HistorySessionListAdapter extends AppBaseAdapter<Person, HistorySessionListAdapterViewHolder> {

    public HistorySessionListAdapter(AppBaseAdapterData<Person> appBaseAdapterData, GroupBaseActivity activity) {
        super(appBaseAdapterData, activity);
    }

    @Override
    public HistorySessionListAdapterViewHolder createViewHolder(View view) {
        HistorySessionListAdapterViewHolder viewHolder = new HistorySessionListAdapterViewHolder();
        viewHolder.displayNameView = (TextView) view.findViewById(R.id.group_item_history_display_name_view);
        viewHolder.pointsView = (TextView) view.findViewById(R.id.group_item_history_points_view);
        viewHolder.presentCheckView = (CheckBox) view.findViewById(R.id.group_item_history_present_view);
        return viewHolder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Person person = (Person) getItem(position);
        convertView = prepareViewAndHolder(convertView, R.layout.group_item_history_layout);
        mViewHolder.displayNameView.setText(person.getDisplayName());
        String pointsValue = "0";
        boolean wasPresentAtSession = false;
        Iterator<PersonSessionResultVO> personSessionResultVOIterator = person.getResults().iterator();
        while (personSessionResultVOIterator.hasNext()) {
            PersonSessionResultVO personSessionResultVO = personSessionResultVOIterator.next();
            if (personSessionResultVO != null) {
                if (personSessionResultVO.timestamp == person.getSelectedHistoryTimestamp()) {
                    pointsValue = String.valueOf(personSessionResultVO.result);
                    wasPresentAtSession = personSessionResultVO.wasAtSession;
                    break;
                }
            }
        }
        mViewHolder.pointsView.setText(pointsValue);
        mViewHolder.presentCheckView.setChecked(wasPresentAtSession);
        return convertView;
    }
}