package chernyshov.yuriy.coachassistant.views;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

import chernyshov.yuriy.coachassistant.BuildConfig;
import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.FlurryManager;
import chernyshov.yuriy.coachassistant.business.database.PersonsDataBaseManager;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.BitmapUtilities;
import chernyshov.yuriy.coachassistant.utilities.ImageLoader;
import chernyshov.yuriy.coachassistant.utilities.IntentUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.vo.PersonEntry;
import chernyshov.yuriy.coachassistant.vo.RequestCodeVO;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 1/3/13
 * Time: 11:08 PM
 */
public final class PersonInfoActivity extends AppBaseActivity {

    private static final byte DIALOG_MODE_ADD = 1;
    private static final byte DIALOG_MODE_EDIT = 2;

    private static Uri sUploadPictureFileUri;
    private Person person;
    private ImageLoader mImageLoader; // Handles loading the contact image in a background thread

    /**
     * Factory method to create {@link Intent} to start {@link PersonInfoActivity}.
     *
     * @param context    Context of the callee.
     * @param personHash Hash number of the {@link Person}.
     *
     * @return {@link Intent}.
     */
    public static Intent makeStartActivityIntent(final Context context, final int personHash) {
        final Intent intent = new Intent(context, PersonInfoActivity.class);
        intent.putExtra(ExtrasVO.PERSON_HASH, personHash);
        return intent;
    }

    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_info_layout);
        final Intent intent = getIntent();
        Bundle bundle = null;
        if (intent != null) {
            bundle = intent.getExtras();
        }
        if (bundle == null) {
            Logger.w("PersonInfo bundle null");
            return;
        }
        final int personHash = bundle.getInt(ExtrasVO.PERSON_HASH);
        person = PersonsDataBaseManager.getPerson(personHash, this);
        Logger.d("Person Info for: " + person);
        if (person == null) {
            return;
        }

        /*
         * An ImageLoader object loads and resizes an image in the background and binds it to the
         * QuickContactBadge in each item layout of the ListView. ImageLoader implements memory
         * caching for each image, which substantially improves refreshes of the ListView as the
         * user scrolls through it.
         *
         * To learn more about downloading images asynchronously and caching the results, read the
         * Android training class Displaying Bitmaps Efficiently.
         *
         * http://developer.android.com/training/displaying-bitmaps/
         */
        mImageLoader = new ImageLoader(this, getListPreferredItemHeight()) {
            @Override
            protected Bitmap processBitmap(Object data) {
                // This gets called in a background thread and passed the data from ImageLoader.loadImage().
                if (data instanceof String) {
                    if (data.toString().startsWith("content:")) {
                        // Instantiates an AssetFileDescriptor. Given a content Uri pointing to an image file, the
                        // ContentResolver can return an AssetFileDescriptor for the file.
                        AssetFileDescriptor assetFileDescriptor = null;
                        // This "try" block catches an Exception if the file descriptor returned from the Contacts
                        // Provider doesn't point to an existing file.
                        try {

                            // Retrieves a file descriptor from the Contacts Provider. To learn more about this
                            // feature, read the reference documentation for
                            // ContentResolver#openAssetFileDescriptor.
                            assetFileDescriptor = getContentResolver().openAssetFileDescriptor(Uri.parse(data.toString()), "r");

                            // Gets a FileDescriptor from the AssetFileDescriptor. A BitmapFactory object can
                            // decode the contents of a file pointed to by a FileDescriptor into a Bitmap.
                            FileDescriptor fileDescriptor = assetFileDescriptor.getFileDescriptor();

                            if (fileDescriptor != null) {
                                // Decodes a Bitmap from the image pointed to by the FileDescriptor, and scales it
                                // to the specified width and height
                                return ImageLoader.decodeSampledBitmapFromDescriptor(fileDescriptor,
                                        (int) (128 * MainApp.RESDENSITY), (int) (128 * MainApp.RESDENSITY));
                            }
                        } catch (FileNotFoundException e) {
                            // If the file pointed to by the thumbnail URI doesn't exist, or the file can't be
                            // opened in "read" mode, ContentResolver.openAssetFileDescriptor throws a
                            // FileNotFoundException.
                            if (BuildConfig.DEBUG) {
                                Logger.e("Contact photo thumbnail not found for contact " + data.toString() + ": "
                                        + e.toString());
                            }
                        } finally {
                            // If an AssetFileDescriptor was returned, try to close it
                            if (assetFileDescriptor != null) {
                                try {
                                    assetFileDescriptor.close();
                                } catch (IOException e) {
                                    // Closing a file descriptor might cause an IOException if the file is
                                    // already closed. Nothing extra is needed to handle this.
                                }
                            }
                        }
                    } else {
                        return BitmapUtilities.decodeSampledBitmapFromFile((String) data,
                                (int) (128 * MainApp.RESDENSITY), (int) (128 * MainApp.RESDENSITY));
                    }
                }
                return null;
            }
        };

        // Set a placeholder loading image for the image loader
        mImageLoader.setLoadingImage(R.drawable.main_img_placeholder);

        // Add a cache to the image loader
        mImageLoader.addImageCache(getSupportFragmentManager(), 0.1f);

        final TextView personNameView = (TextView) findViewById(R.id.person_info_name_view);
        personNameView.setText(person.getDisplayName());

        final ImageView personPicture = (ImageView) findViewById(R.id.person_info_img_view);
        personPicture.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                createPersonIconContextMenu();
                return false;
            }
        });
        if (!TextUtils.isEmpty(person.getPictureURL())) {
            setPersonImage(person.getPictureURL());
        } else if (!TextUtils.isEmpty(person.getContactThumbURI())) {
            setPersonImage(person.getContactThumbURI());
        }

        final TextView phoneView = (TextView) findViewById(R.id.person_info_phone_view);
        phoneView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                createPhoneViewContextMenu();
                return false;
            }
        });
        if (!TextUtils.isEmpty(person.getPhone())) {
            phoneView.setText(person.getPhone());
        }

        final TextView addressView = (TextView) findViewById(R.id.person_info_address_view);
        if (!TextUtils.isEmpty(person.getAddress())) {
            addressView.setText(person.getAddress());
        }
        addressView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                createAddressDialog(TextUtils.equals(addressView.getText().toString().trim(),
                        getString(R.string.person_info_address_holder_text)) ?
                    DIALOG_MODE_ADD : DIALOG_MODE_EDIT);
                return false;
            }
        });

        final TextView descriptionView = (TextView) findViewById(R.id.person_info_description_view);
        if (!TextUtils.isEmpty(person.getDescription())) {
            descriptionView.setText(person.getDescription());
        }
        descriptionView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                createAdditionalInfoDialog(
                        TextUtils.equals(descriptionView.getText().toString().trim(),
                        getString(R.string.person_info_description_holder_text)) ?
                        DIALOG_MODE_ADD : DIALOG_MODE_EDIT
                );
                return false;
            }
        });
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
        //requestCode = (requestCode & 0xffff);
        if (resultCode == android.app.Activity.RESULT_OK) {
            String mediaFilePath = null;
            if (requestCode == RequestCodeVO.PERSON_IMG_CAMERA_RESULT) {
                Logger.d("PersonInfoActivity onActivityResult CAMERA result uri: " + sUploadPictureFileUri);
                if (sUploadPictureFileUri != null) {
                    mediaFilePath = AppUtilities.getMediaPath(sUploadPictureFileUri);
                    sUploadPictureFileUri = null;
                }
                Logger.d("PersonInfoActivity onActivityResult CAMERA result path: " + mediaFilePath);
                setPersonImage(mediaFilePath);
                updatePersonPicture(mediaFilePath);
            }
            if (requestCode == RequestCodeVO.PERSON_IMG_GALLERY_RESULT) {
                Uri uri = data.getData();
                if (uri != null) {
                    mediaFilePath = AppUtilities.getMediaPath(uri);
                    setPersonImage(mediaFilePath);
                    updatePersonPicture(mediaFilePath);
                }
            }
        }
    }

    @Override
    public final void onPause() {
        super.onPause();

        // In the case onPause() is called during a fling the image loader is
        // un-paused to let any remaining background work complete.
        mImageLoader.setPauseWork(false);
    }

    /**
     * Gets the preferred height for each item in the ListView, in pixels, after accounting for
     * screen density. ImageLoader uses this value to resize thumbnail images to match the ListView
     * item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    private int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new android.util.DisplayMetrics();

        // Populate the DisplayMetrics
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }

    private void createPhoneViewContextMenu() {
        Logger.d("Context menu for phone view");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.phone_dialog_header));
        final int dataId;
        if (TextUtils.isEmpty(person.getPhone())) {
           dataId = R.array.add_phone_context_menu;
        } else {
            dataId = R.array.phone_context_menu;
        }
        builder.setItems(dataId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        if (dataId == R.array.add_phone_context_menu) {
                            createPhoneDialog(DIALOG_MODE_ADD);
                        } else {
                            FlurryManager.logEvent(FlurryManager.EVENT_ID_PERSON_INFO_CALL_SELECTED);

                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + person.getPhone()));
                            startActivity(callIntent);
                        }
                        break;
                    case 1:
                        createPhoneDialog(DIALOG_MODE_EDIT);
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createPersonIconContextMenu() {
        Logger.d("Context menu for person icon");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.person_picture_context_menu_header));
        builder.setItems(R.array.main_img_context_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent getGalleryImageIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        getGalleryImageIntent.setType("image/*");
                        getGalleryImageIntent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(getGalleryImageIntent, RequestCodeVO.PERSON_IMG_GALLERY_RESULT);
                        break;
                    case 1:
                        sUploadPictureFileUri = IntentUtilities.createGetPictureIntent(PersonInfoActivity.this,
                                PersonInfoActivity.this, RequestCodeVO.PERSON_IMG_CAMERA_RESULT);
                        break;
                    case 2:
                        setPersonImage(null);
                        ContentValues values = new ContentValues();
                        values.put(PersonEntry.COLUMN_NAME_IMG_URL, "");
                        PersonsDataBaseManager.updatePersonDetails(person.getDisplayName(), PersonInfoActivity.this,
                                values);
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createPhoneDialog(byte dialogMode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        final EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
        editText.setInputType(InputType.TYPE_CLASS_PHONE);
        if (!TextUtils.isEmpty(person.getPhone())) {
            editText.setText(person.getPhone().trim());
        }
        String positiveLabel = getString(R.string.add);
        if (dialogMode == DIALOG_MODE_EDIT) {
            positiveLabel = getString(R.string.apply);
        }
        builder.setView(view)
                .setCancelable(false)
                .setTitle(getResources().getString(R.string.phone_dialog_header))
                .setPositiveButton(positiveLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String personPhone = editText.getText().toString().trim();
                        Logger.d("Update person phone: " + personPhone);
                        TextView phoneView = (TextView) findViewById(R.id.person_info_phone_view);
                        if (phoneView != null) {
                            phoneView.setText(personPhone);
                        }
                        ContentValues values = new ContentValues();
                        values.put(PersonEntry.COLUMN_NAME_PHONE, personPhone);
                        boolean result = PersonsDataBaseManager.updatePersonDetails(person.getDisplayName(),
                                PersonInfoActivity.this, values);
                        if (result) {
                            person.setPhone(personPhone);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_string), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createAddressDialog(byte dialogMode) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        final LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        final EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
        if (!TextUtils.isEmpty(person.getAddress())) {
            editText.setText(person.getAddress().trim());
        }
        String positiveLabel = getString(R.string.add);
        if (dialogMode == DIALOG_MODE_EDIT) {
            positiveLabel = getString(R.string.apply);
        }
        builder.setView(view)
                .setCancelable(false)
                .setTitle(getResources().getString(R.string.address_dialog_header))
                .setPositiveButton(positiveLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final String personAddress = editText.getText().toString().trim();
                        Logger.d("Update person address: " + personAddress);
                        final TextView addressView = (TextView) findViewById(R.id.person_info_address_view);
                        if (addressView != null) {
                            addressView.setText(personAddress);
                        }
                        final ContentValues values = new ContentValues();
                        values.put(PersonEntry.COLUMN_NAME_ADDRESS, personAddress);
                        boolean result = PersonsDataBaseManager.updatePersonDetails(
                                person.getDisplayName(),
                                PersonInfoActivity.this,
                                values
                        );
                        if (result) {
                            person.setAddress(personAddress);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_string), null);
        builder.create().show();
    }

    private void createAdditionalInfoDialog(final byte dialogMode) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        final LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        final EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
        if (!TextUtils.isEmpty(person.getDescription())) {
            editText.setText(person.getDescription().trim());
        }
        String positiveLabel = getString(R.string.add);
        if (dialogMode == DIALOG_MODE_EDIT) {
            positiveLabel = getString(R.string.apply);
        }
        builder.setView(view)
                .setCancelable(false)
                .setTitle(getResources().getString(R.string.description_dialog_header))
                .setPositiveButton(positiveLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final String additionalInfo = editText.getText().toString().trim();
                        Logger.d("Update person additional info: " + additionalInfo);
                        final TextView additionalInfoView
                                = (TextView) findViewById(R.id.person_info_description_view);
                        if (additionalInfoView != null) {
                            additionalInfoView.setText(additionalInfo);
                        }
                        final ContentValues values = new ContentValues();
                        values.put(PersonEntry.COLUMN_NAME_DESCRIPTION, additionalInfo);
                        boolean result = PersonsDataBaseManager.updatePersonDetails(
                                person.getDisplayName(),
                                PersonInfoActivity.this,
                                values
                        );
                        if (result) {
                            person.setDescription(additionalInfo);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_string), null);
        builder.create().show();
    }

    private void updatePersonPicture(String mediaFilePath) {
        ContentValues values = new ContentValues();
        values.put(PersonEntry.COLUMN_NAME_IMG_URL, mediaFilePath);
        PersonsDataBaseManager.updatePersonDetails(person.getDisplayName(), this, values);
    }

    private void setPersonImage(Object mediaFilePath) {
        ImageView personPicture = (ImageView) findViewById(R.id.person_info_img_view);
        if (personPicture == null) {
            return;
        }
        mImageLoader.loadImage(mediaFilePath, personPicture);
    }
}