package chernyshov.yuriy.coachassistant.views.adapters;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/19/12
 * Time: 4:45 PM
 */
public class GroupListAdapterViewHolder {
    TextView displayNameView;
    TextView pointsView;
    ImageView photoView;
}