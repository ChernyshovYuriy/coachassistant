package chernyshov.yuriy.coachassistant.views.adapters;

import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapterData;
import chernyshov.yuriy.coachassistant.model.Person;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/19/12
 * Time: 5:34 PM
 */
public class NewSessionListAdapterData extends AppBaseAdapterData<Person> {

    public NewSessionListAdapterData() {
        super(null);
    }

    public void fillItems(List<Person> persons) {
        clearItems();
        Person previousContact = null;
        for (Person person : persons) {
            doAddItem(person, previousContact);
            previousContact = person;
        }
    }

    private void doAddItem(Person contact, Person previousContact) {
        //noinspection StatementWithEmptyBody
        if (previousContact == null) {
            // TODO:
        }
        super.addItem(contact);
    }
}