package chernyshov.yuriy.coachassistant.views;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.FlurryManager;
import chernyshov.yuriy.coachassistant.business.RateManager;
import chernyshov.yuriy.coachassistant.business.database.AppSettingsDataBaseManager;
import chernyshov.yuriy.coachassistant.business.database.GroupsDataBaseManager;
import chernyshov.yuriy.coachassistant.utilities.*;
import chernyshov.yuriy.coachassistant.vo.AppEntry;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.vo.RequestCodeVO;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppBaseActivity {

    private static Uri uploadPictureFileUri;

    private String selectedGroup = "";
    private ArrayAdapter<String> adapter;
    private Spinner spinner;
    private Button startBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final ImageView mainIcon = (ImageView) findViewById(R.id.main_view_mian_icon);
        mainIcon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                createMainImageContextMenu();
                return false;
            }
        });
        setMainImage(
                AppSettingsDataBaseManager.getAppParameter_String(
                        this,
                        AppEntry.COLUMN_NAME_MAIN_ICON_URL
                )
        );

        final String[] array = {getResources().getString(R.string.new_group_spinner_add_label)};
        final ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.addAll(Arrays.asList(array));
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, stringArrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        startBtn = (Button) findViewById(R.id.main_view_start_btn);
        if (startBtn != null) {
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FlurryManager.logEvent(
                            FlurryManager.EVENT_ID_GROUP_SELECTED,
                            "total-goups",
                            String.valueOf(adapter.getCount())
                    );

                    Intent intent = new Intent(MainActivity.this, GroupViewActivity.class);
                    intent.putExtra(ExtrasVO.GROUP_NAME, spinner.getSelectedItem().toString());
                    //Logger.d("Selected group is " + spinner.getSelectedItem());
                    startActivity(intent);
                }
            });
        }

        selectedGroup = AppSettingsDataBaseManager.getAppParameter_String(
                this,
                AppEntry.COLUMN_NAME_LAST_USED_GROUP
        );
        Logger.d("Selected group onCreate " + selectedGroup);

        spinner = (Spinner) findViewById(R.id.main_view_groups_spinner);
        if (spinner != null) {
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Logger.d("Group selected " + adapter.getItem(i));
                    processSelection(adapter.getItem(i));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Logger.d("Group nothing selected");
                }
            });
        }

        //DataBaseHelper.dumpDatabase(this, DataBaseConstants.GROUPS_TABLE_NAME);
        //DataBaseHelper.dumpDatabase(this, DataBaseConstants.PERSONS_TABLE_NAME);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAdapter();
        Logger.d("Selected group onResume " + selectedGroup);
        int selectedId = adapter.getPosition(selectedGroup);
        if (selectedId != -1) {
            spinner.setSelection(adapter.getPosition(selectedGroup));
        }
        RateManager.getInstance().incrementCounter(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ContentValues values = new ContentValues();
        values.put(AppEntry.COLUMN_NAME_LAST_USED_GROUP, adapter.getItem((int) spinner.getSelectedItemId()));
        AppSettingsDataBaseManager.setAppParameter(this, values);
        /*if (!StringUtils.isEmpty(AppSettingsDataBaseManager.getAppParameter_String(this, AppEntry.COLUMN_NAME_LAST_USED_GROUP))) {
            AppSettingsDataBaseManager.updateAppParameter(this, values);
        } else {
            AppSettingsDataBaseManager.addAppParameter(this, values);
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_view_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Logger.d("MainActivity options menu click '" + item.getTitle() + "'");
        switch (item.getItemId()) {
            case R.id.main_view_about_menu_item:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
        //requestCode = (requestCode & 0xffff);
        if (resultCode == android.app.Activity.RESULT_OK) {
            String mediaFilePath = null;
            if (requestCode == RequestCodeVO.MAIN_IMG_CAMERA_RESULT) {
                Logger.d("MainActivity onActivityResult CAMERA result uri: " + uploadPictureFileUri);
                if (uploadPictureFileUri != null) {
                    mediaFilePath = AppUtilities.getMediaPath(uploadPictureFileUri);
                    uploadPictureFileUri = null;
                }
                Logger.d("MainActivity onActivityResult CAMERA result path: " + mediaFilePath);
                setMainImage(mediaFilePath);
                addOrUpdateMainImageInDB(mediaFilePath);
            }
            if (requestCode == RequestCodeVO.MAIN_IMG_GALLERY_RESULT) {
                Uri uri = data.getData();
                if (uri != null) {
                    mediaFilePath = AppUtilities.getMediaPath(uri);
                    setMainImage(mediaFilePath);
                    addOrUpdateMainImageInDB(mediaFilePath);
                }
            }
        }
    }

    private void createMainImageContextMenu() {
        Logger.d("Context menu for main icon");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.main_icon_context_menu_header));
        builder.setItems(R.array.main_img_context_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent getGalleryImageIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        getGalleryImageIntent.setType("image/*");
                        getGalleryImageIntent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(getGalleryImageIntent, RequestCodeVO.MAIN_IMG_GALLERY_RESULT);
                        break;
                    case 1:
                        uploadPictureFileUri = IntentUtilities.createGetPictureIntent(MainActivity.this,
                                MainActivity.this,
                                RequestCodeVO.MAIN_IMG_CAMERA_RESULT);
                        break;
                    case 2:
                        setMainImage(null);
                        ContentValues values = new ContentValues();
                        values.put(AppEntry.COLUMN_NAME_MAIN_ICON_URL, "");
                        AppSettingsDataBaseManager.setAppParameter(MainActivity.this, values);
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addOrUpdateMainImageInDB(String mediaFilePath) {
        ContentValues values = new ContentValues();
        values.put(AppEntry.COLUMN_NAME_MAIN_ICON_URL, mediaFilePath);
        AppSettingsDataBaseManager.setAppParameter(this, values);
    }

    private void setMainImage(String mediaFilePath) {
        TextView textView = (TextView) findViewById(R.id.main_view_main_icon_desc);
        ImageView mainIcon = (ImageView) findViewById(R.id.main_view_mian_icon);
        if (!TextUtils.isEmpty(mediaFilePath)) {
            Bitmap bitmap = BitmapUtilities.decodeSampledBitmapFromFile(mediaFilePath,
                    (int) (128 * MainApp.RESDENSITY), (int) (128 * MainApp.RESDENSITY));
            if (bitmap != null) {
                mainIcon.setImageBitmap(bitmap);
                textView.setVisibility(View.INVISIBLE);
            }
        } else {
            mainIcon.setImageDrawable(getResources().getDrawable(R.drawable.main_img_placeholder));
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void processSelection(String groupName) {
        String initValue = (String) getResources().getText(R.string.new_group_spinner_add_label);
        if (TextUtils.equals(groupName, initValue)) {
            Logger.d("Create new group");
            startBtn.setEnabled(false);
            createNewGroupDialog();
        } else {
            selectedGroup = adapter.getItem((int) spinner.getSelectedItemId());
            startBtn.setEnabled(true);
        }
    }

    private void createNewGroupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        builder.setView(view)
                .setCancelable(false)
                .setTitle(getResources().getString(R.string.new_group_label))
                .setPositiveButton(getString(R.string.new_group_ok_string), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
                        if (editText != null) {
                            String groupName = editText.getText().toString().trim();
                            int result = GroupsDataBaseManager.addGroup(groupName, MainActivity.this);
                            if (result != -1) {
                                updateAdapter();
                                startBtn.setEnabled(true);
                                if (spinner != null && adapter != null) {
                                    spinner.setSelection(adapter.getPosition(groupName));
                                }
                            } else {
                                createNewGroupDialog();
                            }
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_string), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateAdapter() {
        ArrayList<String> stringArrayList = GroupsDataBaseManager.readGroups(this);
        if (stringArrayList != null && stringArrayList.size() > 0) {
            adapter.clear();
            for (int i = 0; i < stringArrayList.size(); i++) {
                adapter.add(stringArrayList.get(i));
            }
            adapter.add(getResources().getString(R.string.new_group_spinner_add_label));
        }
    }
}