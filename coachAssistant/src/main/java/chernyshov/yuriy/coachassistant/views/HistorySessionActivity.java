package chernyshov.yuriy.coachassistant.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.ListView;
import android.widget.TextView;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.utilities.Logger;
import chernyshov.yuriy.coachassistant.views.adapters.HistorySessionListAdapter;
import chernyshov.yuriy.coachassistant.views.adapters.NewSessionListAdapterData;
import chernyshov.yuriy.coachassistant.vo.ExtrasVO;
import chernyshov.yuriy.coachassistant.model.Person;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/26/12
 * Time: 3:16 PM
 */
public class HistorySessionActivity extends GroupBaseActivity {

    private final NewSessionListAdapterData mHistorySessionListAdapterData = new NewSessionListAdapterData();
    private final HistorySessionListAdapter mHistorySessionListAdapter = new HistorySessionListAdapter(mHistorySessionListAdapterData, this);
    private long historyTimeStamp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_history_session_layout);

        Intent intent = getIntent();
        Bundle bundle = null;
        if (intent != null) {
            bundle = intent.getExtras();
        }

        if (bundle != null) {
            TextView headerView = (TextView) findViewById(R.id.group_history_session_header);
            if (headerView != null) {
                headerView.setText(Html.fromHtml(getHeaderString()));
            }
            TextView dateText = (TextView) findViewById(R.id.group_history_session_date);
            String date;
            if (bundle.containsKey(ExtrasVO.HISTORY_TIMESTAMP_STRING) && bundle.containsKey(ExtrasVO.HISTORY_TIMESTAMP)) {
                date = bundle.getString(ExtrasVO.HISTORY_TIMESTAMP_STRING);
                historyTimeStamp = bundle.getLong(ExtrasVO.HISTORY_TIMESTAMP);
                dateText.setText(date);
            } else {
                Logger.e("HistorySessionActivity timestamp NULL");
                return;
            }
        }

        ListView listView = (ListView) findViewById(R.id.group_history_session_list);
        listView.setAdapter(mHistorySessionListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<Person> arrayList = loadPersons();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Person person = arrayList.get(i);
            if (person != null) {
                person.setSelectedHistoryTimestamp(historyTimeStamp);
            }
        }
        mHistorySessionListAdapterData.fillItems(arrayList);
        mHistorySessionListAdapter.notifyDataSetChanged();
    }
}