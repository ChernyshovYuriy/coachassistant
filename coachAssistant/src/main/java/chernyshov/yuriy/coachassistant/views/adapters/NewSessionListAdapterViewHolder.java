package chernyshov.yuriy.coachassistant.views.adapters;

import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/19/12
 * Time: 5:35 PM
 */
public class NewSessionListAdapterViewHolder {
    TextView displayNameView;
    TextView pointsView;
    CheckBox presentCheckView;
}