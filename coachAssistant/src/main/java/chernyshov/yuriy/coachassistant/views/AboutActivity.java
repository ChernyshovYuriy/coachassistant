package chernyshov.yuriy.coachassistant.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.utilities.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10.01.13
 * Time: 16:09
 */
public class AboutActivity extends AppBaseActivity {

    private static final String AUTHOR_URL = "http://www.linkedin.com/pub/yuriy-chernyshov/1b/622/270";
    private static final String APP_SOURCE_URL = "https://bitbucket.org/ChernyshovYuriy/coachassistantnew";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_layout);

        TextView versionView = (TextView) findViewById(R.id.about_app_version_view);
        versionView.setText(getString(R.string.app_version_appendix) + " " + AppUtilities.getApplicationVersion());

        TextView authorLinkView = (TextView) findViewById(R.id.about_app_author_url_name);
        authorLinkView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(AUTHOR_URL));
                try {
                    startActivity(intent);
                } catch (Throwable throwable) {
                    Logger.e("Can not open author URL", throwable);
                    SafeToast.showToastAnyThread(getString(R.string.create_intent_error));
                }
            }
        });

        TextView appSourceView = (TextView) findViewById(R.id.app_open_source_link_view);
        appSourceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(APP_SOURCE_URL));
                try {
                    startActivity(intent);
                } catch (Throwable throwable) {
                    Logger.e("Can not open app source URL", throwable);
                    SafeToast.showToastAnyThread(getString(R.string.create_intent_error));
                }
            }
        });

        TextView emailView = (TextView) findViewById(R.id.about_app_email_view);
        emailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("mailto:chernyshov.yuriy@gmail.com");
                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Coach Assistant customer comments / questions");
                startActivity(intent);
            }
        });
    }
}