package chernyshov.yuriy.coachassistant.views.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import chernyshov.yuriy.coachassistant.MainApp;
import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapter;
import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapterData;
import chernyshov.yuriy.coachassistant.utilities.AppUtilities;
import chernyshov.yuriy.coachassistant.views.GroupBaseActivity;
import chernyshov.yuriy.coachassistant.views.SafeToast;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.vo.PersonSessionResultVO;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/19/12
 * Time: 2:20 PM
 */
public class NewSessionListAdapter extends AppBaseAdapter<Person, NewSessionListAdapterViewHolder> {

    public NewSessionListAdapter(AppBaseAdapterData<Person> appBaseAdapterData, GroupBaseActivity activity) {
        super(appBaseAdapterData, activity);
    }

    @Override
    public NewSessionListAdapterViewHolder createViewHolder(View view) {
        NewSessionListAdapterViewHolder viewHolder = new NewSessionListAdapterViewHolder();
        viewHolder.displayNameView = (TextView) view.findViewById(R.id.group_item_new_session_display_name_view);
        viewHolder.pointsView = (TextView) view.findViewById(R.id.group_item_new_session_points_view);
        viewHolder.presentCheckView = (CheckBox) view.findViewById(R.id.group_item_new_session_present_view);
        return viewHolder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Person person = (Person) getItem(position);
        convertView = prepareViewAndHolder(convertView, R.layout.group_item_new_session_layout);
        mViewHolder.displayNameView.setText(person.getDisplayName());
        String pointsValue = "0";
        boolean wasAtSession = false;
        long currentDayTimestamp = AppUtilities.startOfDay(System.currentTimeMillis());
        Iterator<PersonSessionResultVO> personSessionResultVOIterator = person.getResults().iterator();
        while (personSessionResultVOIterator.hasNext()) {
            PersonSessionResultVO personSessionResultVO = personSessionResultVOIterator.next();
            if (personSessionResultVO != null) {
                if (personSessionResultVO.timestamp == currentDayTimestamp) {
                    pointsValue = String.valueOf(personSessionResultVO.result);
                    wasAtSession = personSessionResultVO.wasAtSession;
                    break;
                }
            }
        }
        mViewHolder.pointsView.setText(pointsValue);
        mViewHolder.pointsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSelectResultDialog(person);
            }
        });
        mViewHolder.presentCheckView.setChecked(Boolean.TRUE.equals(wasAtSession));
        mViewHolder.presentCheckView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processCheckboxClickForDB(v, person);
            }
        });
        return convertView;
    }

    private void processCheckboxClickForDB(View v, Person person) {
        if (v == null || person == null) {
            return;
        }
        CheckBox checkBox = (CheckBox) v.findViewById(R.id.group_item_new_session_present_view);
        boolean newCheckboxState = checkBox.isChecked();
        mCurrentActivity.setPersonPresence(person, newCheckboxState);
    }

    private void createSelectResultDialog(final Person person) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCurrentActivity);
        // Get the layout inflater
        android.view.LayoutInflater inflater = (LayoutInflater) mCurrentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        final EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
        if (editText != null) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        builder.setView(view)
                .setCancelable(false)
                .setTitle(mCurrentActivity.getResources().getString(R.string.select_result_dialog_title))
                .setPositiveButton(mCurrentActivity.getString(R.string.new_result_ok_string), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (editText != null) {
                            String stringValue = editText.getText().toString();
                            if (!TextUtils.isEmpty(stringValue) && Integer.valueOf(stringValue) < 127 && Integer.valueOf(stringValue) > -127) {
                                mCurrentActivity.setPersonResult(person, Byte.valueOf(stringValue));
                                MainApp.getInstance().runInUIThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        notifyDataSetChanged();
                                    }
                                });
                            } else {
                                SafeToast.showToastAnyThread(MainApp.getInstance().getResources().getString(R.string.incorrect_person_result));
                            }
                        }
                    }
                })
                .setNegativeButton(mCurrentActivity.getString(R.string.cancel_string), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}