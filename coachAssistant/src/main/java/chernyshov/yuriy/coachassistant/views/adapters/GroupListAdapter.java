package chernyshov.yuriy.coachassistant.views.adapters;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import chernyshov.yuriy.coachassistant.R;
import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapter;
import chernyshov.yuriy.coachassistant.business.views.AppBaseAdapterData;
import chernyshov.yuriy.coachassistant.model.Person;
import chernyshov.yuriy.coachassistant.utilities.ImageLoader;
import chernyshov.yuriy.coachassistant.views.GroupBaseActivity;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 03.12.12
 * Time: 22:02
 */
public class GroupListAdapter extends AppBaseAdapter<Person, GroupListAdapterViewHolder> {

    ImageLoader mImageLoader;

    public GroupListAdapter(AppBaseAdapterData<Person> appBaseAdapterData, GroupBaseActivity activity) {
        super(appBaseAdapterData, activity);
    }

    public void setImageLoader(ImageLoader mImageLoader) {
        this.mImageLoader = mImageLoader;
    }

    @Override
    public GroupListAdapterViewHolder createViewHolder(View view) {
        GroupListAdapterViewHolder viewHolder = new GroupListAdapterViewHolder();
        viewHolder.displayNameView = (TextView) view.findViewById(R.id.group_item_display_name_view);
        viewHolder.pointsView = (TextView) view.findViewById(R.id.group_item_points_view);
        viewHolder.photoView = (ImageView) view.findViewById(R.id.group_item_avatar_view);
        return viewHolder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Person person = (Person) getItem(position);
        convertView = prepareViewAndHolder(convertView, R.layout.group_item_general_layout);
        mViewHolder.displayNameView.setText(person.getDisplayName());

        // TODO: Workaround!!!
        mImageLoader.loadImage(null, mViewHolder.photoView);

        if (!TextUtils.isEmpty(person.getPictureURL())) {
            mImageLoader.loadImage(person.getPictureURL(), mViewHolder.photoView);
        } else if (!TextUtils.isEmpty(person.getContactThumbURI())) {
            mImageLoader.loadImage(person.getContactThumbURI(), mViewHolder.photoView);
        }
        int value = 0;
        if (person.getResults() != null) {
            for (int i = 0; i < person.getResults().size(); i++) {
                value += person.getResults().get(i).result;
            }
        }
        mViewHolder.pointsView.setText(String.valueOf(value));
        return convertView;
    }
}