CoachAssistant
==============

This is a simple android application for the coach. It allows you to manage groups of students, as well as information
on each student. Up to now it is possible to manage only unique students in groups, in other words it is no possible
to have a student with same name in different groups (suggestion up to now to give an unique name for such student).